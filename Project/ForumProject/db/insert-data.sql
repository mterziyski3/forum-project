INSERT INTO forumdatabase.categories (category_id, name)
VALUES (1, 'Fifa World Cup 2022 General Discussion');

INSERT INTO forumdatabase.categories (category_id, name)
VALUES (2, 'Teams');

INSERT INTO forumdatabase.categories (category_id, name)
VALUES (3, 'Matches');

INSERT INTO forumdatabase.categories (category_id, name)
VALUES (4, 'News');

INSERT INTO forumdatabase.categories (category_id, name)
VALUES (5, 'Groups');

INSERT INTO forumdatabase.user_roles (role_id, name)
VALUES (1, 'user');

INSERT INTO forumdatabase.user_roles (role_id, name)
VALUES (2, 'administrator');

INSERT INTO forumdatabase.users (user_id, first_name, last_name, username,
                                 password, email_address, is_blocked, role_id)
VALUES (1, 'Ivan', 'Petrov', 'ivan55', '12345', 'ivan_petrov@gmail.com', false, 1);

INSERT INTO forumdatabase.users (user_id, first_name, last_name, username,
                                 password, email_address, is_blocked, role_id)
VALUES (2, 'Georgi', 'Ivanov', 'joro89', 'joro12345', 'joro_ivanov@gmail.com', false, 2);

INSERT INTO forumdatabase.users (user_id, first_name, last_name, username, password, email_address, is_blocked, role_id)
VALUES (3, 'Gosho', 'Goshev', 'gosho39', 'gosho1234', 'gosho39@abv.bg', false, 2);

INSERT INTO forumdatabase.users (user_id, first_name, last_name, username, password, email_address, is_blocked, role_id)
VALUES (4, 'Pesho', 'Peshev', 'pesho39', 'pesho1234', 'pesho39@abv.bg', false, 1);

INSERT INTO u852m20po87f2oxh.posts (post_id, title, content, user_id, category_id, creation_date)
VALUES (1, 'Mondial 2022 Finals', 'Who will play in the finals?', 1, 2, current_time);

INSERT INTO u852m20po87f2oxh.posts (post_id, title, content, user_id, category_id, creation_time)
VALUES (2, 'Neymar, Mbappe and Cristiano', 'Who will be  Qatar 2022''s striker', 3, 1, current_time);

INSERT INTO u852m20po87f2oxh.posts (post_id, title, content, user_id, category_id, creation_time)
VALUES (3, 'Group A', 'Qatar, Ecuador, Senegal, and Netherlands - schedule, fixtures, rankings', 4, 5, current_time);

INSERT INTO u852m20po87f2oxh.posts (post_id, title, content, user_id, category_id, creation_time)
VALUES (4, 'Group B', 'England to face Iran, Wales will be the United States'' first opponents at the World Cup', 5, 5, current_time);

INSERT INTO u852m20po87f2oxh.posts (post_id, title, content, user_id, category_id, creation_time)
VALUES (5, 'Group C', 'Argentina, Saudi Arabia, Mexico, Poland: an intriguing group of teams with very different styles of play.', 3, 5, current_time);