create table categories
(
    category_id int auto_increment
        primary key,
    name        varchar(50) not null
);

create table tags
(
    tag_id int auto_increment
        primary key,
    name   varchar(20) not null
);

create table user_roles
(
    role_id int auto_increment
        primary key,
    name    varchar(20) not null
);

create table users
(
    user_id       int auto_increment
        primary key,
    first_name    varchar(32) not null,
    last_name     varchar(32) not null,
    username      varchar(32) not null,
    password      varchar(32) not null,
    email_address varchar(32) not null,
    is_blocked    tinyint(1)  not null,
    role_id       int         null,
    constraint users_email_address_uindex
        unique (email_address),
    constraint users_user_roles_fk
        foreign key (role_id) references user_roles (role_id)
);

create table phone_numbers
(
    phone_number_id int auto_increment
        primary key,
    phone_number    varchar(15) null,
    user_id         int         null,
    constraint phone_numbers_users_fk
        foreign key (user_id) references users (user_id)
);

create table posts
(
    post_id       int auto_increment
        primary key,
    title         varchar(100)  not null,
    content       varchar(5000) not null,
    user_id       int           not null,
    category_id   int           not null,
    creation_date datetime      not null,
    likes_count   int(255)      null,
    constraint posts_categories_fk
        foreign key (category_id) references categories (category_id),
    constraint posts_users_fk
        foreign key (user_id) references users (user_id)
);

create table likes
(
    user_id int null,
    post_id int null,
    constraint likes_posts_fk
        foreign key (post_id) references posts (post_id),
    constraint likes_users_fk
        foreign key (user_id) references users (user_id)
);

create table posts_tags
(
    post_id int null,
    tag_id  int null,
    constraint posts_tags_posts_fk
        foreign key (post_id) references posts (post_id),
    constraint posts_tags_tags_fk
        foreign key (tag_id) references tags (tag_id)
);

create table replies
(
    reply_id      int auto_increment
        primary key,
    post_id       int          not null,
    user_id       int          not null,
    content       varchar(500) not null,
    likes_count   int          not null,
    creation_date datetime     not null,
    constraint replies_posts_fk
        foreign key (post_id) references posts (post_id),
    constraint replies_users_fk
        foreign key (user_id) references users (user_id)
);

