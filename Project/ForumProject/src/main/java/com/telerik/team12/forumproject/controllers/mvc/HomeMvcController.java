package com.telerik.team12.forumproject.controllers.mvc;

import com.telerik.team12.forumproject.controllers.AuthenticationHelper;
import com.telerik.team12.forumproject.exceptions.AuthenticationFailureException;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.services.contracts.CategoryService;
import com.telerik.team12.forumproject.services.contracts.PostService;
import com.telerik.team12.forumproject.services.contracts.TagService;
import com.telerik.team12.forumproject.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final UserService userService;
    private final CategoryService categoryService;
    private final TagService tagService;

    private final PostService postService;

    private final AuthenticationHelper authenticationHelper;


    public HomeMvcController(UserService userService,
                             CategoryService categoryService,
                             TagService tagService,
                             PostService postService, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.categoryService = categoryService;
        this.tagService = tagService;
        this.postService = postService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession httpSession) {
        return httpSession.getAttribute("currentUser") != null;
    }

    @GetMapping
    public String showHomePage(HttpSession httpSession, Model model) {
        if (httpSession.getAttribute("currentUser") != null) {
            String username = httpSession.getAttribute("currentUser").toString();
            User user = userService.getByUsername(username);
            model.addAttribute("user", user);
            model.addAttribute("categories", categoryService.getAllCategories());

        }
        model.addAttribute("categories", categoryService.getAllCategories().subList(0, 6));
        model.addAttribute("users", userService.getUsers().subList(0, 6));
        model.addAttribute("tags", tagService.getTags().subList(0, 6));

        model.addAttribute("topTen", postService.getTopTen());
        model.addAttribute("latestTen", postService.getLatestTen());
        model.addAttribute("hottestTen", postService.getHottestTen());
        return "IndexView";
    }

    @GetMapping("/admin")
    public String showAdminPortal(HttpSession session, Model model) {
        try {
            User currentUser = authenticationHelper.tryGetUser(session);

            if (currentUser.checkIfUserAdmin()) {
                return "admin-portal";
            }
        } catch (AuthenticationFailureException e) {
        }

        model.addAttribute("error", "You are not authorized to access the requested resource.");
        return "access-denied";
    }


}
