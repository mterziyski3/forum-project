package com.telerik.team12.forumproject.models;

import java.util.Optional;

public class FilterOptionsCategory {

    private Optional<String> name;
    private Optional<String> sortBy;
    private Optional<String> sortOrder;

    public FilterOptionsCategory(Optional<String> name,
                                 Optional<String> sortBy,
                                 Optional<String> sortOrder) {
        this.name = name;
        this.sortBy = sortBy;
        this.sortOrder = sortOrder;
    }

    public Optional<String> getName() {
        return name;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }
}
