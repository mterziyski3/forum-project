package com.telerik.team12.forumproject.services;

import com.telerik.team12.forumproject.exceptions.DuplicateEntityException;
import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.exceptions.UnauthorizedOperationException;
import com.telerik.team12.forumproject.models.FilterOptionsUser;
import com.telerik.team12.forumproject.models.Post;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.models.User_Role;
import com.telerik.team12.forumproject.repositories.contracts.UserRepository;
import com.telerik.team12.forumproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {
    private static final String MODIFY_USER_ERROR_MESSAGE = "Only admin can modify a user.";

    private UserRepository repository;

    @Autowired
    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<User> filter(FilterOptionsUser filterOptions) {
        return repository.filter(filterOptions);
    }

    @Override
    public Set<Post> getUserLikedPosts(int id) {
        return repository.getUserLikedPosts(id);
    }

    @Override
    public List<User> getUsers() {
        return repository.getUsers();
    }

    @Override
    public User getUser(int id) {
        return repository.getUser(id);
    }

    @Override
    public User createUser(User user) {
        boolean duplicateUsernameExists = true;
        boolean duplicateEmailExists = true;

        try{
            User existingUserWithEmail = repository.getUserByEmail(user.getEmail());
        }catch(EntityNotFoundException e) {
            duplicateEmailExists = false;
        }

        try{
            User existingUserWithUsername = repository.getUserByUsername(user.getUsername());
        }catch(EntityNotFoundException e) {
            duplicateUsernameExists = false;
        }

        if(duplicateEmailExists && duplicateUsernameExists) {
            throw new DuplicateEntityException("User", "Email address, Username", user.getEmail());
        }

        else if(duplicateUsernameExists) {
            throw new DuplicateEntityException("User", "Username", user.getUsername());
        }

        else if(duplicateEmailExists) {
            throw new DuplicateEntityException("User", "Email Address", user.getUsername());
        }

        repository.createUser(user);
        return user;
    }

    @Override
    public User updateUser(User user, User userWhoIsUpdating) {
        if (!userWhoIsUpdating.checkIfUserAdmin() && user.getId() != userWhoIsUpdating.getId()) {
            throw new UnauthorizedOperationException(MODIFY_USER_ERROR_MESSAGE);
        }

        boolean duplicateUsernameExists = true;
        boolean duplicateEmailExists = true;

        try{
            User existingUserWithEmail = repository.getUserByEmail(user.getEmail());
            if(user.getId() == existingUserWithEmail.getId()) {
                duplicateEmailExists = false;
            }
        }catch(EntityNotFoundException e) {
            duplicateEmailExists = false;
        }

        try{
            User existingUserWithUsername = repository.getUserByUsername(user.getUsername());
            if(user.getId() == existingUserWithUsername.getId()) {
                duplicateUsernameExists = false;
            }
        }catch(EntityNotFoundException e) {
            duplicateUsernameExists = false;
        }

        if(duplicateEmailExists && duplicateUsernameExists) {
            throw new DuplicateEntityException("User", "Email address, Username", user.getEmail());
        }

        else if(duplicateUsernameExists) {
            throw new DuplicateEntityException("User", "Username", user.getUsername());
        }

        else if(duplicateEmailExists) {
            throw new DuplicateEntityException("User", "Email Address", user.getUsername());
        }

        repository.updateUser(user);
        return user;
    }

    @Override
    public User deleteUser(int id, User userWhoIsUpdating) {
        if (!userWhoIsUpdating.checkIfUserAdmin()) {
            throw new UnauthorizedOperationException(MODIFY_USER_ERROR_MESSAGE);
        }

        return repository.deleteUser(id);
    }

    @Override
    public User getByUsername(String username) {
        return repository.getUserByUsername(username);
    }

    @Override
    public User getByEmail(String email) {
        return repository.getUserByEmail(email);
    }

    @Override
    public User getById(int id) {
        return repository.getUserById(id);
    }

    @Override
    public void blockUser(int id, User userWhoIsBlocking) {
        if(!userWhoIsBlocking.checkIfUserAdmin()) {
            throw new UnauthorizedOperationException(MODIFY_USER_ERROR_MESSAGE);
        }
        repository.blockUser(id);
    }
    @Override
    public void unblockUser(int id, User userWhoIsBlocking) {
        if(!userWhoIsBlocking.checkIfUserAdmin()) {
            throw new UnauthorizedOperationException(MODIFY_USER_ERROR_MESSAGE);
        }
        repository.unblockUser(id);
    }

    @Override
    public void makeAdmin(int id, User userWhoIsUpdating) {
        if(!userWhoIsUpdating.checkIfUserAdmin()) {
            throw new UnauthorizedOperationException(MODIFY_USER_ERROR_MESSAGE);
        }

        User userToUpdate = getById(id);
        User_Role role = new User_Role("Admin");
        role.setId(1);
        userToUpdate.setRole(role);

        repository.makeAdmin(userToUpdate);
    }
}
