package com.telerik.team12.forumproject.models.Dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class UserDto {

    private int id;
    @NotNull
    @Size(min = 4, max = 32, message = "First name's length should be between 4 and 32 symbols.")
    private String firstName;
    @NotNull
    @Size(min = 4, max = 32, message = "Last name's length should be between 4 and 32 symbols.")
    private String lastName;

    private String username;
    @Email
    private String email;

    public UserDto() {

    }

    public UserDto(int id, String firstName, String lastName, String username, String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
