package com.telerik.team12.forumproject.services;

import com.telerik.team12.forumproject.exceptions.DuplicateEntityException;
import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.exceptions.UnauthorizedOperationException;
import com.telerik.team12.forumproject.models.*;
import com.telerik.team12.forumproject.repositories.contracts.PostRepository;
import com.telerik.team12.forumproject.repositories.contracts.TagRepository;
import com.telerik.team12.forumproject.services.contracts.PostService;
import com.telerik.team12.forumproject.services.contracts.TagService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class PostServiceImpl implements PostService {

    private final PostRepository postRepository;

    private final TagRepository tagRepository;
    private final TagService tagService;

    public PostServiceImpl(PostRepository postRepository, TagRepository tagRepository, TagService tagService) {
        this.postRepository = postRepository;
        this.tagRepository = tagRepository;
        this.tagService = tagService;
    }

    @Override
    public List<Post> getAllForAdmins(User user) {
        if (!user.checkIfUserAdmin()) {
            throw new UnauthorizedOperationException("Only admins can access the full list of posts.");
        }
        return postRepository.getAll();
    }

    @Override
    public List<Post> filter(FilterOptionsPosts filterOptionsPosts, User user) {
        if (user.getIsBlocked()) {
            throw new UnauthorizedOperationException("Only active users can filter posts.");
        }
        return postRepository.filter(filterOptionsPosts);
    }

    @Override
    public List<Post> getLatestTen() {
        return postRepository.getLatestTen();
    }

    @Override
    public List<Post> getHottestTen() {
        return postRepository.getHottestTen();
    }

    @Override
    public List<Post> getTopTen() {
        return postRepository.getTopTen();
    }

    @Override
    public Post getById(int id, User user) {
        if (user.getIsBlocked()) {
            throw new UnauthorizedOperationException("Only active users can search posts by id.");
        }

        return postRepository.getById(id);
    }

    public Post getById(int id) {
        return postRepository.getById(id);
    }

    @Override
    public List<Post> getPosts() {
        return postRepository.getPosts();
    }

    @Override
    public Set<Reply> getRepliesByPostId(int postId) {
        return postRepository.getRepliesByPostId(postId);
    }

    @Override
    public Set<Tag> getTagsByPostId(int postId) {
        return postRepository.getTagsByPostId(postId);
    }

    @Override
    public Post getByTitle(String title, User user) {
        return postRepository.getByTitle(title);
    }

    @Override
    public Post createPost(Post post, User user) {
        if (user.getIsBlocked()) {
            throw new UnauthorizedOperationException("Only active users can create a post.");
        }
        boolean duplicateExists = true;
        try {
            postRepository.getByTitle(post.getTitle());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Post", "title", post.getTitle());
        }
        postRepository.createPost(post);

        return post;
    }

    @Override
    public void update(Post post, User user) {
        if (!user.checkIfUserAdmin() && post.getCreatedBy().getId() != user.getId() && user.getIsBlocked()) {
            throw new UnauthorizedOperationException("Only admin or the creator, if not blocked, can modify a post.");
        }
        boolean duplicateExists = true;
        try {
            Post existingPost = postRepository.getByTitle(post.getTitle());
            if (existingPost.getId() == post.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Post", "title", post.getTitle());
        }
        postRepository.update(post);

    }

    @Override
    public void delete(int id, User user) {
        Post postToDelete = postRepository.getById(id);
        if (!user.checkIfUserAdmin() && postToDelete.getCreatedBy().getId() != user.getId() && user.getIsBlocked()) {
            throw new UnauthorizedOperationException("Only admin or the creator, if not blocked, can delete a post.");
        }
        postRepository.delete(id);
    }

    @Override
    public void toggleLike(int id, User user) {
        postRepository.toggleLike(id, user);
    }

    public void toggleTag(int id, Tag tag){
        postRepository.toggleTag(id, tag);
    }

    public void addTag(int postId, Tag tag, User user) {
        Post postToBeTagged = postRepository.getById(postId);
        if (!user.checkIfUserAdmin() && postToBeTagged.getCreatedBy().getId() != user.getId()
                && user.getIsBlocked()) {
            throw new UnauthorizedOperationException
                    ("Only admin or the creator, if not blocked, can tag a post.");
        }
        boolean duplicateExists = true;
        try {
            tagRepository.getTagByName(tag.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
            throw new EntityNotFoundException("Tag", "tag name", tag.getName());
           // tagRepository.createTag(tag);
        }
        Tag tagToBeAdded = tagService.getByName(tag.getName());
        if (postToBeTagged.getPostTags().contains(tagToBeAdded)) {
            throw new DuplicateEntityException("Tag", "name",tagToBeAdded.getName());
        }
        postToBeTagged.getPostTags().add(tagToBeAdded);
        tagToBeAdded.getTaggedPosts().add(postToBeTagged);
        postRepository.update(postToBeTagged);
        tagRepository.updateTag(tagToBeAdded);
    }
}
