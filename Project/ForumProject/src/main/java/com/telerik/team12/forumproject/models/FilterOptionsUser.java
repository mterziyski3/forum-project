package com.telerik.team12.forumproject.models;

import java.util.Optional;

public class FilterOptionsUser {
    private Optional<String> firstName;
    private Optional<String> lastName;
    private Optional<String> userName;
    private Optional<String> sortBy;
    private Optional<String> sortOrder;

    public FilterOptionsUser(Optional<String> firstName, Optional<String> lastName, Optional<String> userName, Optional<String> sortBy, Optional<String> sortOrder) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.sortBy = sortBy;
        this.sortOrder = sortOrder;
    }

    public Optional<String> getFirstName() {
        return firstName;
    }

    public Optional<String> getLastName() {
        return lastName;
    }

    public Optional<String> getUserName() {
        return userName;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }
}

