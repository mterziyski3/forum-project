package com.telerik.team12.forumproject.controllers.rest;

import com.telerik.team12.forumproject.controllers.AuthenticationHelper;
import com.telerik.team12.forumproject.exceptions.DuplicateEntityException;
import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.exceptions.UnauthorizedOperationException;
import com.telerik.team12.forumproject.models.Dtos.ReplyDTO;
import com.telerik.team12.forumproject.models.Dtos.ReplyDtoOut;
import com.telerik.team12.forumproject.models.Dtos.ReplyDtoOutNormalUser;
import com.telerik.team12.forumproject.models.FilterOptionsReplies;
import com.telerik.team12.forumproject.models.Reply;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.services.contracts.ReplyService;
import com.telerik.team12.forumproject.services.mappers.ReplyMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/posts/replies")
public class ReplyController {

    private final ReplyService replyService;

    private final ReplyMapper replyMapper;

    private final AuthenticationHelper authenticationHelper;

    public ReplyController(ReplyService replyService, ReplyMapper replyMapper, AuthenticationHelper authenticationHelper) {
        this.replyService = replyService;
        this.replyMapper = replyMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/admin")
    private List<Reply> getAllReplies(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return replyService.getAllReplies(user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("admin/{id}")
    public ReplyDtoOut getById(@PathVariable int id) {
        try {
            Reply reply = replyService.getById(id);
            return replyMapper.objectToDTO(reply);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping ("/{id}")
    public ReplyDtoOutNormalUser getByIdForNormalUser(@PathVariable int id) {
        try {
            Reply reply = replyService.getById(id);
            return replyMapper.objectToDTOForNormalUser(reply);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

//    @GetMapping("/filter")
//    public List<Reply> filter(
//            @RequestParam(required = false) Optional<String> content,
//            @RequestParam(required = false) Optional<Integer> userId,
//            @RequestParam(required = false) Optional<Integer> postId,
//            @RequestParam(required = false) Optional<LocalDateTime> creationTime,
//            @RequestParam(required = false) Optional<String> sortBy,
//            @RequestParam(required = false) Optional<String> sortOrder) {
//        return replyService.filter(new FilterOptionsReplies(content, userId, postId, creationTime, sortBy, sortOrder));
//    }

//    @PostMapping
//    public ReplyDTO create(@RequestHeader HttpHeaders headers, @Valid @RequestBody ReplyDTO replyDTO) {
//        try {
//            User user = authenticationHelper.tryGetUser(headers);
//            Reply reply = replyMapper.dtoToObject(replyDTO, new Reply());
//            reply.setCreatedBy(reply.getCreatedBy());
//            replyService.create(reply, user);
//            return replyDTO;
//        } catch (DuplicateEntityException e) {
//            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
//        } catch (UnauthorizedOperationException e) {
//            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
//        }
//    }

//    @PutMapping("/{id}")
//    public ReplyDTO update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody ReplyDTO replyDTO) {
//        try {
//            User user = authenticationHelper.tryGetUser(headers);
//            Reply replyToUpdate = replyService.getById(id);
//            Reply reply = replyMapper.dtoToObject(replyDTO, replyToUpdate);
//            replyService.update(reply, user);//don`t throw duplicate exception
//            return replyDTO;
//        } catch (EntityNotFoundException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        } catch (DuplicateEntityException e) {
//            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
//        } catch (UnauthorizedOperationException e) {
//            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
//        }
//    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            replyService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
