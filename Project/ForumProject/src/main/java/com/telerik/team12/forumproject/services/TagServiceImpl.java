package com.telerik.team12.forumproject.services;

import com.telerik.team12.forumproject.exceptions.DuplicateEntityException;
import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.models.FilterOptionsTags;
import com.telerik.team12.forumproject.models.Post;
import com.telerik.team12.forumproject.models.Tag;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.repositories.contracts.TagRepository;
import com.telerik.team12.forumproject.repositories.contracts.UserRepository;
import com.telerik.team12.forumproject.services.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class TagServiceImpl implements TagService {
    private TagRepository repository;

    @Autowired
    public TagServiceImpl(TagRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Tag> filter(FilterOptionsTags filterOptionsTags) {
        return repository.filter(filterOptionsTags);
    }

    @Override
    public List<Tag> getTags() {
        return repository.getTags();
    }

    @Override
    public Tag getTag(int id) {
        return repository.getTag(id);
    }

    @Override
    public Tag createTag(Tag tag) {
        boolean duplicateExists = true;
        try{
            Tag existingTag = repository.getTagByName(tag.getName());
        }catch(EntityNotFoundException e) {
            duplicateExists = false;
        }

        if(duplicateExists) {
            throw new DuplicateEntityException("Tag", "Name", tag.getName());
        }

        repository.createTag(tag);
        return tag;
    }

    @Override
    public Tag updateTag(Tag tag) {
        boolean duplicateExists = true;

        try{
            Tag existingTag = repository.getTagByName(tag.getName());
            if(tag.getId() == existingTag.getId()) {
                duplicateExists = false;
            }
        }catch(EntityNotFoundException e) {
            duplicateExists = false;
        }

        if(duplicateExists) {
            throw new DuplicateEntityException("Tag", "Name", tag.getName());
        }

        repository.updateTag(tag);
        return tag;
    }

    @Override
    public Tag deleteTag(int id) {
        return repository.deleteTag(id);
    }

    @Override
    public Tag getByName(String name) {
        return repository.getTagByName(name);
    }

    @Override
    public Set<Post> getTaggedPostsByTagId(int tagId, User user) {
        return repository.getTaggedPostsByTagId(tagId);
    }
}
