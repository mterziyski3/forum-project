package com.telerik.team12.forumproject.services;

import com.telerik.team12.forumproject.exceptions.DuplicateEntityException;
import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.exceptions.UnauthorizedOperationException;
import com.telerik.team12.forumproject.models.FilterOptionsReplies;
import com.telerik.team12.forumproject.models.Post;
import com.telerik.team12.forumproject.models.Reply;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.repositories.contracts.ReplyRepository;
import com.telerik.team12.forumproject.services.contracts.PostService;
import com.telerik.team12.forumproject.services.contracts.ReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReplyServiceImpl implements ReplyService {


    private static final String CREATE_REPLY_ERROR_MESSAGE = "Only admin or user can create a reply.";
    private static final String GET_ALL_REPLY_ERROR_MESSAGE = "Only admin can list all replies.";
    private static final String BLOCK_REPLY_ERROR_MESSAGE = "You have been blocked!";
    private static final String MODIFY_REPLY_ERROR_MESSAGE = "Only admin or author can modify a reply.";
    private static final String DELETE_REPLY_ERROR_MESSAGE = "Only admin or author can delete a reply.";
    private final ReplyRepository replyRepository;

    private final PostService postService;

    @Autowired
    public ReplyServiceImpl(ReplyRepository replyRepository, PostService postService) {
        this.replyRepository = replyRepository;
        this.postService = postService;
    }

    @Override
    public List<Reply> getAllReplies(User user) {
        if (!user.checkIfUserAdmin()) {
            throw new UnauthorizedOperationException(GET_ALL_REPLY_ERROR_MESSAGE);
        }
        return replyRepository.getAllReplies();
    }

    @Override
    public Reply getById(int id) {
        return replyRepository.getById(id);
    }

    @Override
    public Reply create(Post post, Reply reply, User user) {
        if (!user.checkIfUserAdmin() && !user.checkIfUserNormal()) {
            throw new UnauthorizedOperationException(CREATE_REPLY_ERROR_MESSAGE);
        }
        if (user.getIsBlocked()) {
            throw new UnauthorizedOperationException(BLOCK_REPLY_ERROR_MESSAGE);
        }
        boolean duplicateExists = true;
        try {
            replyRepository.getByContent(reply.getContent());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Reply", "name", reply.getContent());
        }
        reply.setCreatedBy(user);
        replyRepository.create(reply);
        return reply;
    }

    @Override
    public Reply update(Reply reply, User user) {
        if (!user.checkIfUserAdmin() && !reply.getCreatedBy().equals(user)) {
            throw new UnauthorizedOperationException(MODIFY_REPLY_ERROR_MESSAGE);
        }
        boolean duplicateExists = true;
        try {
            Reply existingReply = replyRepository.getByContent(reply.getContent());
           if (existingReply.getId() == reply.getId()) {
               duplicateExists = false;
           }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Reply", "name", reply.getContent());
        }
        return replyRepository.update(reply);
    }

    @Override
    public void delete(int id, User user) {
        Reply reply = replyRepository.getById(id);
        if (!user.checkIfUserAdmin() && !reply.getCreatedBy().equals(user)) {
            throw new UnauthorizedOperationException(DELETE_REPLY_ERROR_MESSAGE);
        }
        if (user.getIsBlocked()) {
            throw new UnauthorizedOperationException(BLOCK_REPLY_ERROR_MESSAGE);
        }
        replyRepository.delete(id);
    }

    @Override
    public List<Reply> filter(User user, FilterOptionsReplies filterOptionsReplies) {
        if (!user.checkIfUserAdmin()) {
            throw new UnauthorizedOperationException(GET_ALL_REPLY_ERROR_MESSAGE);
        }
        return replyRepository.filter(filterOptionsReplies);
    }
}
