package com.telerik.team12.forumproject.models;

import java.time.LocalDateTime;
import java.util.Optional;

public class FilterOptionsReplies {
    private Optional<String> content;

    private Optional<String> createdBy;
    private Optional<LocalDateTime> creationTime;

    private Optional<String> sortBy;

    private Optional<String> sortOrder;

    public FilterOptionsReplies(
            Optional<String> content,
            Optional<String> createdBy,
            Optional<LocalDateTime> creationTime,
            Optional<String> sortBy,
            Optional<String> sortOrder) {
        this.content = content;
        this.createdBy = createdBy;
        this.creationTime = creationTime;
        this.sortBy = sortBy;
        this.sortOrder = sortOrder;
    }


    public Optional<String> getContent() {
        return content;
    }

    public Optional<String> getCreatedBy() {
        return createdBy;
    }
    public Optional<LocalDateTime> getCreationTime() {
        return creationTime;
    }

    public Optional<String> getSortBy() {
            return sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }
}
