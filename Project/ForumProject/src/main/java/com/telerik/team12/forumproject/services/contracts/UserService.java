package com.telerik.team12.forumproject.services.contracts;

import com.telerik.team12.forumproject.models.FilterOptionsUser;
import com.telerik.team12.forumproject.models.Post;
import com.telerik.team12.forumproject.models.FilterOptionsUser;
import com.telerik.team12.forumproject.models.User;

import java.util.List;
import java.util.Set;

public interface UserService {
    List<User> getUsers();

    User getUser(int id);

    User createUser(User user);

    User updateUser(User user, User userWhoIsUpdating);

    User deleteUser(int id, User userWhoIsUpdating);

    User getByUsername(String username);

    Set<Post> getUserLikedPosts(int id);
    List<User> filter(FilterOptionsUser filterOptions);

    User getByEmail(String email);

    User getById(int id);

    void blockUser(int id, User userWhoIsBlocking);

    void unblockUser(int id, User userWhoIsBlocking);

    void makeAdmin(int id, User userWhoIsUpdating);
}
