package com.telerik.team12.forumproject.repositories;

import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.models.Category;
import com.telerik.team12.forumproject.models.FilterOptionsCategory;
import com.telerik.team12.forumproject.repositories.contracts.CategoryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CategoryRepositoryImpl implements CategoryRepository {

    private final SessionFactory sessionFactory;

    public CategoryRepositoryImpl(SessionFactory sessionFactory) {

        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Category> getAllCategories() {
         try (Session session = sessionFactory.openSession()) {
             Query<Category> query = session.createQuery("from Category", Category.class);
             return query.list();
         }
    }

    @Override
    public List<Category> filter(FilterOptionsCategory filterOptionsCategory) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from Category where 1=1");

            if (filterOptionsCategory.getName().isPresent()) {
                queryString.append(" and name like :name");
            }

            addSorting(filterOptionsCategory, queryString);

            Query<Category> query = session.createQuery(queryString.toString(), Category.class);

            if (filterOptionsCategory.getName().isPresent()) {
                query.setParameter("name", String.format("%%%s%%", filterOptionsCategory.getName().get()));
            }
            return query.list();
        }
    }

    @Override
    public Category getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Category category = session.get(Category.class, id);
            if (category == null) {
                throw new EntityNotFoundException("Category", id);
            }
            return category;
        }
    }

    @Override
    public Category getByName(String name) {
       try (Session session = sessionFactory.openSession()) {
            Query<Category> query = session.createQuery("from Category where name = :name", Category.class);
            query.setParameter("name", name);
            List<Category> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Category", "name", name);
            }
            return result.get(0);
       }
    }

    @Override
    public Category create(Category category) {
        try (Session session = sessionFactory.openSession()) {
            session.save(category);
        }
        return category;
    }

    @Override
    public Category update(Category category) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(category);
            session.getTransaction().commit();
        }
        return category;
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            Category categoryToDelete = getById(id);
            session.beginTransaction();
            session.delete(categoryToDelete);
            session.getTransaction().commit();
        }
    }

    private void addSorting(FilterOptionsCategory filterOptionsCategory, StringBuilder queryString) {
        if (filterOptionsCategory.getSortBy().isEmpty()) {
            return;
        }
        String orderBy = "";
        switch (filterOptionsCategory.getSortBy().get()) {
            case "name":
                orderBy = "name";
                break;
            default:
                return;
        }
        queryString.append(String.format(" order by %s", orderBy));

        if (filterOptionsCategory.getSortOrder().isPresent() &&
                filterOptionsCategory.getSortOrder().get().equalsIgnoreCase("desc")) {
            queryString.append(" desc");
        }
    }
}
