package com.telerik.team12.forumproject.controllers.rest;

import com.telerik.team12.forumproject.controllers.AuthenticationHelper;
import com.telerik.team12.forumproject.exceptions.*;
import com.telerik.team12.forumproject.models.Dtos.UserDto;
import com.telerik.team12.forumproject.models.FilterOptionsUser;
import com.telerik.team12.forumproject.models.Post;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.services.mappers.UserMapper;
import com.telerik.team12.forumproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService service;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UserService service, UserMapper userMapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/filter")
    public List<User> filter(
            @RequestParam(required = false) Optional<String> firstName,
            @RequestParam(required = false) Optional<String> lastName,
            @RequestParam(required = false) Optional<String> userName,
            @RequestParam(required = false) Optional<String> sortBy,
            @RequestParam(required = false) Optional<String> sortOrder) {
        return service.filter(new FilterOptionsUser(firstName, lastName, userName,
                sortBy, sortOrder));
    }

    @GetMapping
    public List<User> getUsers() {
        return service.getUsers();
    }

    @GetMapping("/{id}")
    public User getUser(@PathVariable @Valid int id) {
        try {
            return service.getUser(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public User createUser(@RequestHeader HttpHeaders headers, @RequestBody UserDto userDto) {
        try {
            User user = userMapper.fromDto(userDto, new User());
            service.createUser(user);
            return user;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
//        } catch (UnauthorizedOperationException e) {
//            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public User updateUser(@RequestHeader HttpHeaders headers, @PathVariable int id, @RequestBody @Valid UserDto userDto) {
        try {
            User userWhoIsUpdating = authenticationHelper.tryGetUser(headers);
            User userToUpdate = service.getUser(userDto.getId());

            User user = userMapper.fromDto(userDto, userToUpdate);
            service.updateUser(user, userWhoIsUpdating);

            return user;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }

    @DeleteMapping("/{id}")
    public User deleteUser(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User userWhoIsUpdating = authenticationHelper.tryGetUser(headers);
            return service.deleteUser(id, userWhoIsUpdating);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());

        }
    }

    @GetMapping("/likedPosts/{id}")
    public Set<Post> getUserLikedPosts(@PathVariable int id) {
        try {
            return service.getUserLikedPosts(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/block/{id}")
    public void blockUser(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User userWhoIsBlocking = authenticationHelper.tryGetUser(headers);
            service.blockUser(id, userWhoIsBlocking);
        } catch(EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch(UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/unblock/{id}")
    public void unblockUser(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User userWhoIsUnblocking = authenticationHelper.tryGetUser(headers);
            service.unblockUser(id, userWhoIsUnblocking);
        } catch(EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch(UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/make-admin/{id}")
    public void makeAdmin(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User userWhoIsUpdating = authenticationHelper.tryGetUser(headers);
            service.makeAdmin(id, userWhoIsUpdating);
        } catch(EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch(UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
