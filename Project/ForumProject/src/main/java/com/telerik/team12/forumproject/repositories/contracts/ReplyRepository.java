package com.telerik.team12.forumproject.repositories.contracts;

import com.telerik.team12.forumproject.models.FilterOptionsReplies;
import com.telerik.team12.forumproject.models.Post;
import com.telerik.team12.forumproject.models.Reply;
import com.telerik.team12.forumproject.models.User;

import java.util.List;

public interface ReplyRepository {

    List<Reply> getAllReplies();

    List<Reply> filter(FilterOptionsReplies filterOptionsReplies);

    Reply getById(int id);

    Reply getByContent(String content);

    Reply create(Reply reply);

    Reply update(Reply reply);

    void delete(int id);


}
