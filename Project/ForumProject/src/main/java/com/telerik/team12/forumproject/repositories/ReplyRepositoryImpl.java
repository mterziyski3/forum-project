package com.telerik.team12.forumproject.repositories;

import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.models.FilterOptionsReplies;
import com.telerik.team12.forumproject.models.Reply;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.repositories.contracts.ReplyRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReplyRepositoryImpl implements ReplyRepository {

    private final SessionFactory sessionFactory;


    public ReplyRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Reply> getAllReplies() {
        try (Session session  = sessionFactory.openSession()) {
            Query<Reply> queue = session.createQuery("from Reply", Reply.class);
            return queue.list();
        }
    }

    @Override
    public List<Reply> filter(FilterOptionsReplies filterOptionsReplies) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from Reply where 1=1");

            if (filterOptionsReplies.getContent().isPresent()) {
                queryString.append(" and content like :content");
            }
            if (filterOptionsReplies.getCreatedBy().isPresent()) {
                queryString.append(" and createdBy.username = :createdByUsername");
            }
//            if (filterOptionsReplies.getPost_id().isPresent()) {
//                queryString.append(" and post.id = :postId");
//            }
            if (filterOptionsReplies.getCreationTime().isPresent()) {
                queryString.append(" and creationTime like :creationTime");
            }

            addSorting(filterOptionsReplies, queryString);

            Query<Reply> query = session.createQuery(queryString.toString(), Reply.class);

            if (filterOptionsReplies.getContent().isPresent()) {
                query.setParameter("content", String.format("%%%s%%", filterOptionsReplies.getContent().get()));
            }
            if (filterOptionsReplies.getCreatedBy().isPresent()) {
                query.setParameter("createdByUsername", filterOptionsReplies.getCreatedBy().get());
            }
//            if (filterOptionsReplies.getPost_id().isPresent()) {
//                query.setParameter("postId", filterOptionsReplies.getPost_id().get());
//            }
            if (filterOptionsReplies.getCreationTime().isPresent()) {
                query.setParameter("creationTime", filterOptionsReplies.getCreationTime().get());
            }

            return query.list();
        }
    }


    private void addSorting(FilterOptionsReplies filterOptionsReplies, StringBuilder queryString) {
            if (filterOptionsReplies.getSortBy().isEmpty()) {
                return;
            }

            String orderBy = "";
            switch (filterOptionsReplies.getSortBy().get()) {
                case "content":
                    orderBy = "content";
                    break;
                case "createdByUsername":
                    orderBy = "createdBy.username";
                    break;
                case "post":
                    orderBy = "post.title";
                    break;
                case "creationTime":
                    orderBy = "creationTime";
                    break;
                default:
                    return;
            }

            queryString.append(String.format(" order by %s", orderBy));

            if (filterOptionsReplies.getSortOrder().isPresent() &&
                    filterOptionsReplies.getSortOrder().get().equalsIgnoreCase("desc")) {
                queryString.append(" desc");
            }
    }

    @Override
    public Reply getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Reply reply = session.get(Reply.class, id);
            if (reply == null) {
                throw new EntityNotFoundException("Reply", id);
            }
            return reply;
        }
    }

    @Override
    public Reply getByContent(String content) {
        try (Session session = sessionFactory.openSession()) {
            Query<Reply> query = session.createQuery("from Reply where content = :content", Reply.class);
            query.setParameter("content", content);
            List<Reply> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Reply", "content", content);
            }
            return result.get(0);
        }
    }

    @Override
    public Reply create(Reply reply) {
        try (Session session = sessionFactory.openSession()) {
            session.save(reply);
        }
         return reply;
    }

    @Override
    public Reply update(Reply reply) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(reply);
            session.getTransaction().commit();
        }
        return reply;
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            Reply replyToDelete = getById(id);
            session.beginTransaction();
            session.delete(replyToDelete);
            session.getTransaction().commit();
        }
    }
}
