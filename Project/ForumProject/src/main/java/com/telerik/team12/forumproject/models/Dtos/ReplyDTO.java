package com.telerik.team12.forumproject.models.Dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.telerik.team12.forumproject.models.Post;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

public class ReplyDTO {

    private int id;
    @NotNull
    @Size(min = 2, max = 500, message = "Reply should be between 5 and 500 symbols.")
    private String content;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime creationTime;

    public ReplyDTO() {

    }

    public ReplyDTO(String content, LocalDateTime creationTime) {
        this.content = content;
        this.creationTime = creationTime;

    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getCreationTime() {
        return LocalDateTime.now();
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }

}
