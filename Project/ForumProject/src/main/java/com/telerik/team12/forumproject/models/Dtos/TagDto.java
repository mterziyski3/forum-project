package com.telerik.team12.forumproject.models.Dtos;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class TagDto {

    private int id;
    @NotNull
    @Size(min = 2, max = 15, message = "Tag's name length should be between 2 and 15 symbols.")
    private String name;

    private String sortBy;

    private String sortOrder;

    public TagDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }
}
