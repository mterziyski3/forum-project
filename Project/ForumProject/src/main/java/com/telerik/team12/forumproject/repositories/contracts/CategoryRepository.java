package com.telerik.team12.forumproject.repositories.contracts;

import com.telerik.team12.forumproject.models.Category;
import com.telerik.team12.forumproject.models.FilterOptionsCategory;

import java.util.List;

public interface CategoryRepository {

    List<Category> getAllCategories();
    List<Category> filter(FilterOptionsCategory filterOptionsCategory);

    Category getById(int id);

    Category getByName(String name);

    Category create(Category category);

    Category update(Category category);

    void delete(int id);
}
