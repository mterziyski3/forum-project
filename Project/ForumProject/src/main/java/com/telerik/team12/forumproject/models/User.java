package com.telerik.team12.forumproject.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;
    @NotNull
    @Size(min = 4, max = 32, message = "First name's length should be between 4 and 32 symbols.")
    @Column(name = "first_name")
    private String firstName;
    @NotNull
    @Size(min = 4, max = 32, message = "Last name's length should be between 4 and 32 symbols.")
    @Column(name = "last_name")
    private String lastName;
    @NotNull
    @Column(name = "username")
    private String username;
    @Email
    @NotNull
    @Column(name = "email_address")
    private String email;
    @NotNull
    @JsonIgnore
    @Column(name = "password")
    private String password;

    @Column(name = "is_blocked")
    private boolean isBlocked;
    @ManyToOne()
    //@Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    @JoinColumn(name = "role_id")
    private User_Role role;


    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_likes",
               joinColumns = @JoinColumn(name = "user_id"),
               inverseJoinColumns = @JoinColumn(name = "post_id"))
    private Set<Post> likedPosts;

    public User() {

    }

    public User(int id, String firstName, String lastName, String email, String username, String password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = password;

        this.role = new User_Role("Normal");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User_Role getRole() {
        return role;
    }

    public void setRole(User_Role role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean getIsBlocked() {
        return isBlocked;
    }

    public void setIsBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public boolean checkIfUserAdmin() {
        return role.getName().equalsIgnoreCase("Admin");
    }

    public boolean checkIfUserNormal() {
        return role.getName().equalsIgnoreCase("Normal");
    }

    public Set<Post> getLikedPosts() {
        return likedPosts;
    }

    public boolean hasLikeInLikedPostsList(int postId) {
        return getLikedPosts().stream().
                map(Post::getId).
                anyMatch(id -> id == postId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() !=  o.getClass()) return false;
        User user = (User) o;
        return username.equals(user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(username);
    }
}
