package com.telerik.team12.forumproject.models.Dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.telerik.team12.forumproject.models.Reply;
import com.telerik.team12.forumproject.models.Tag;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

public class PostDtoAdvancedView {

    @Positive
    private int postId;
    @NotNull(message = "Message cannot be empty")
    @Size(min = 16, max = 64, message = "Title should be between 16 and 64 symbols")
    private String title;
    @NotNull(message = "Message cannot be empty")
    @Size(min = 32, max = 8192, message = "Content should be between 32 and 8192 symbols")
    private String content;
    @NotNull(message = "Message cannot be empty")
    @Size(min = 5, max = 25, message = "Category name should be between 5 and 25 symbols")
    private String categoryName;
    @NotNull(message = "Message cannot be empty")
    private String createdByUsername;
    @NotNull(message = "createdByUsername cannot be empty")
    private String createdByFullName;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime creationTime;
    private int likesCount;
    private List<ReplyDtoOutNormalUser> replies;
    private List<TagDto> postTags;

    public PostDtoAdvancedView() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCreatedByUsername() {
        return createdByUsername;
    }

    public void setCreatedByUsername(String createdByUsername) {
        this.createdByUsername = createdByUsername;
    }

    public String getCreatedByFullName() {
        return createdByFullName;
    }

    public void setCreatedByFullName(String createdByFullName) {
        this.createdByFullName = createdByFullName;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

   public List<ReplyDtoOutNormalUser> getReplies() {
       return replies;
    }

   public void setReplies(List<ReplyDtoOutNormalUser> replies) {
        this.replies = replies;
   }


    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public List<TagDto> getPostTags() {
        return postTags;
    }

    public void setPostTags(List<TagDto> postTags) {
        this.postTags = postTags;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }
}
