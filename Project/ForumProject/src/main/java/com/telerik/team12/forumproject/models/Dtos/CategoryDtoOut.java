package com.telerik.team12.forumproject.models.Dtos;

import java.util.Set;

public class CategoryDtoOut {

    private int id;

    private String name;

    private int postCount;

    private Set<PostDtoOutForCategory> posts;

    private String sortBy;

    private String sortOrder;

    public CategoryDtoOut() {

    }

    public CategoryDtoOut(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPostCount() {
        return postCount;
    }

    public void setPostCount(int postCount) {
        this.postCount = postCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<PostDtoOutForCategory> getPosts() {
        return posts;
    }

    public void setPosts(Set<PostDtoOutForCategory> posts) {
        this.posts = posts;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }
}
