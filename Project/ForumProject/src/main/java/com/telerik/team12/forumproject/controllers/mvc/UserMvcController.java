package com.telerik.team12.forumproject.controllers.mvc;

import com.telerik.team12.forumproject.controllers.AuthenticationHelper;
import com.telerik.team12.forumproject.exceptions.AuthenticationFailureException;
import com.telerik.team12.forumproject.exceptions.DuplicateEntityException;
import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.exceptions.UnauthorizedOperationException;
import com.telerik.team12.forumproject.models.Dtos.UserDto;
import com.telerik.team12.forumproject.models.Dtos.UserFilterDto;
import com.telerik.team12.forumproject.models.FilterOptionsUser;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.services.contracts.UserService;
import com.telerik.team12.forumproject.services.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import com.telerik.team12.forumproject.models.User_Role;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UserMvcController {
    public static final String USER_WITH_THIS_USERNAME_ALREADY_EXISTS = "User with this Username already exists.";
    public static final String USER_WITH_THIS_EMAIL_ADDRESS_ALREADY_EXISTS = "User with this Email Address already exists.";
    private final UserService userService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserMvcController(UserService userService, UserMapper userMapper, AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession httpSession) {
        return httpSession.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isAdmin")
    public boolean populateIsAdmin(HttpSession httpSession) {
        try {
            User user = authenticationHelper.tryGetUser(httpSession);
            return user.checkIfUserAdmin();
        } catch (AuthenticationFailureException e) {
            return false;
        }
    }

    @GetMapping
    public String showUsers(@ModelAttribute("filterOptions") UserFilterDto userFilterDto, Model model) {
        FilterOptionsUser filterOptions = userMapper.fromFilterDto(userFilterDto);
        List<User> users = userService.filter(filterOptions);

        model.addAttribute("filterOptions", userFilterDto);
        model.addAttribute("users", users);
        return "UsersView";
    }


    @GetMapping("/{id}")
    public String showUser(@PathVariable int id, Model model) {
        try {
            User user = userService.getUser(id);
            model.addAttribute("user", user);
            model.addAttribute("userId", user.getId());
            return "UserView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/new")
    public String showNewUserPage(Model model, HttpSession httpSession) {
        try {
            authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("user", new UserDto());

        return "user-new";
    }

    @PostMapping("/new")
    public String createUser(@Valid @ModelAttribute("user") UserDto userDto, BindingResult bindingResult, Model model, HttpSession httpSession) {
        User userWhoIsUpdating;
        try {
            userWhoIsUpdating = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "user-new";
        }

        try {
            User user = userMapper.fromDto(userDto, userWhoIsUpdating);
            userService.createUser(user);

            return "redirect:/users";
        } catch (DuplicateEntityException e) {
            if(e.getMessage().contains("Username")) {
                bindingResult.rejectValue("username", "duplicateUser", USER_WITH_THIS_USERNAME_ALREADY_EXISTS);
            }

            if(e.getMessage().contains("Email")) {
                bindingResult.rejectValue("email", "duplicateUser", USER_WITH_THIS_EMAIL_ADDRESS_ALREADY_EXISTS);
            }

            return "User-New";
        }
    }

    @GetMapping("/{id}/edit")
    public String showEditUserPage(@PathVariable int id, Model model, HttpSession httpSession) {
        User updatingUser;
        try {
            updatingUser = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (!updatingUser.checkIfUserAdmin()) {
            return "Access-Denied";
        }

        try {
            User user = userService.getById(id);
            UserDto userDto = userMapper.toDto(user);
            model.addAttribute("userId", user.getId());
            model.addAttribute("user", userDto);

            return "User-Update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{id}/edit")
    public String editUser(@PathVariable int id, @Valid @ModelAttribute("user") UserDto userDto, BindingResult bindingResult, Model model, HttpSession httpSession) {
        User userWhoIsUpdating;
        try {
            userWhoIsUpdating = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "User-Update";
        }
        try {
            User user = userMapper.fromDto(userDto, new User());
            userService.updateUser(user, userWhoIsUpdating);

            return "redirect:/users";
        } catch (DuplicateEntityException e) {
            if(e.getMessage().contains("Username")) {
                bindingResult.rejectValue("username", "duplicateUser", USER_WITH_THIS_USERNAME_ALREADY_EXISTS);
            }

            if(e.getMessage().contains("Email")) {
                bindingResult.rejectValue("email", "duplicateUser", USER_WITH_THIS_EMAIL_ADDRESS_ALREADY_EXISTS);
            }

            return "User-Update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";

        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());

            return "Access-Denied";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id, Model model, HttpSession httpSession) {
        User userWhoIsUpdating;
        try {
            userWhoIsUpdating = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            userService.deleteUser(id, userWhoIsUpdating);

            return "redirect:/users";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "Access-Denied";

        }
    }
}

