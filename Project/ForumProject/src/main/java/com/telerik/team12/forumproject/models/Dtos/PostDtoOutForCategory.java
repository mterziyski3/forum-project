package com.telerik.team12.forumproject.models.Dtos;

import com.telerik.team12.forumproject.models.Post;

public class PostDtoOutForCategory {

    private String title;


    public PostDtoOutForCategory(Post post) {
        this.title = post.getTitle();
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
