package com.telerik.team12.forumproject.controllers.mvc;


import com.telerik.team12.forumproject.controllers.AuthenticationHelper;
import com.telerik.team12.forumproject.exceptions.AuthenticationFailureException;
import com.telerik.team12.forumproject.exceptions.DuplicateEntityException;
import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.exceptions.UnauthorizedOperationException;
import com.telerik.team12.forumproject.models.Dtos.ReplyDTO;
import com.telerik.team12.forumproject.models.Dtos.ReplyDtoOut;
import com.telerik.team12.forumproject.models.FilterOptionsReplies;
import com.telerik.team12.forumproject.models.Post;
import com.telerik.team12.forumproject.models.Reply;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.services.contracts.PostService;
import com.telerik.team12.forumproject.services.contracts.ReplyService;
import com.telerik.team12.forumproject.services.contracts.UserService;
import com.telerik.team12.forumproject.services.mappers.ReplyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("posts/{postId}/replies")
public class ReplyMvcController {

    private final ReplyService replyService;

    private final UserService userService;

    private final ReplyMapper replyMapper;

    private final PostService postService;

    private final AuthenticationHelper authenticationHelper;


    @Autowired
    public ReplyMvcController(ReplyService replyService, UserService userService, ReplyMapper replyMapper, PostService postService, AuthenticationHelper authenticationHelper) {
        this.replyService = replyService;
        this.userService = userService;
        this.replyMapper = replyMapper;
        this.postService = postService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("IsAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping
    public String showAllReplies(@ModelAttribute("filterOptions") ReplyDtoOut replyDtoOut,
                                 Model model,
                                 HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect/auth/login";
        }
        try {
        FilterOptionsReplies filterOptionsReplies = replyMapper.fromDto(replyDtoOut);
//        List<ReplyDtoOut> replies = replyService.filter(filterOptionsReplies)
//                .stream()
//                .map(replyMapper::toDisplayDto)
//                .collect(Collectors.toList());
        List<Reply> replies = replyService.filter(user, filterOptionsReplies);
        model.addAttribute("replies", replies);
        model.addAttribute("filterOptions", replyDtoOut);
        return "RepliesView";
        } catch (UnauthorizedOperationException e) {
        model.addAttribute("error", e.getMessage());
        return "Access-Denied";
        }
    }

    @GetMapping("/{id}")
    public String showSingleReply(@PathVariable int id, Model model) {
        try {
            Reply reply = replyService.getById(id);
            model.addAttribute("reply", reply);
            return "ReplyView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/new")
    public String showNewReplyPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect/auth/login";
        }
            model.addAttribute("reply", new ReplyDTO());
        return "Reply-New";
    }

    @PostMapping("/new")
    public String createReply(@PathVariable@Valid int postId, @ModelAttribute("reply") ReplyDTO replyDTO,
                                 BindingResult errors,
                                 Model model,
                                 HttpSession session) {

        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect/auth/login";
        }
        if (errors.hasErrors()) {
            return "Reply-New";
        }
        try {
            Post post = postService.getById(postId);
            Reply newReply = replyMapper.dtoToObject(user, post, replyDTO);
            newReply.setCreatedBy(newReply.getCreatedBy());
            replyService.create(post, newReply, user);
//            return "redirect:/posts/{postId}/replies";
            return String.format("redirect:/posts/%d", postId);
        } catch (DuplicateEntityException e) {
            errors.rejectValue("content", "reply.exists", e.getMessage());
            return "Reply-New";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditReplyPage(@PathVariable@Valid int postId, @PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/replies";
        }
        Post post = postService.getById(postId);
        Reply reply = replyService.getById(id);
        model.addAttribute("post", post);
        model.addAttribute("reply", reply);
        return "Reply-Update";
    }

    @PostMapping("/{id}/update")
    public String updateReply(@PathVariable int postId, @PathVariable int id, @Valid @ModelAttribute("reply") ReplyDTO replyDTO,
                              Model model,
                              BindingResult errors,
                              HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect/auth/login";
        }

        if (errors.hasErrors()) {
            return "Reply-Update";
        }
        try {
            Reply replyToUpdate = replyService.getById(id);
            Reply reply = replyMapper.dtoToReply(replyDTO, replyToUpdate);
            replyService.update(reply, user);
            return "redirect:/posts/{postId}/replies";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "reply.exists", e.getMessage());
            return "Reply-Update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "Access-Denied";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteReply(@PathVariable int id, Model model,
                              HttpSession session) {
            User user;
            try {
                user = authenticationHelper.tryGetUser(session);
            } catch (AuthenticationFailureException e) {
                return "redirect/auth/login";
            }
        try {
            replyService.delete(id, user);
            return "redirect:/replies";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "Access-Denied";
        }
    }



}
