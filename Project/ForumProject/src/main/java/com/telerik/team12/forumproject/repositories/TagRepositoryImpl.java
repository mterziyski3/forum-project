package com.telerik.team12.forumproject.repositories;

import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.models.*;
import com.telerik.team12.forumproject.repositories.contracts.TagRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Repository
public class TagRepositoryImpl implements TagRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public TagRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Tag> getTags() {
        try(Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag", Tag.class);
            return query.list();
        }
    }

    @Override
    public Tag getTag(int id) {
        return getTagById(id);
    }

    @Override
    public Tag createTag(Tag tag) {
        try(Session session = sessionFactory.openSession()) {
            session.save(tag);
        }

        return tag;
    }

    @Override
    public Tag updateTag(Tag tag) {
        try(Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(tag);
            session.getTransaction().commit();
        }
        return tag;
    }

    @Override
    public Tag deleteTag(int id) {
        try(Session session = sessionFactory.openSession()){
            Tag tagToDelete = getTagById(id);

            session.beginTransaction();
            session.delete(tagToDelete);
            session.getTransaction().commit();

            return tagToDelete;
        }
    }

    @Override
    public Tag getTagById(int id) {
        try(Session session = sessionFactory.openSession()) {
            Tag tag = session.get(Tag.class, id);

            if(tag == null) {
                throw new EntityNotFoundException("Tag", id);
            }

            return tag;
        }
    }

    @Override
    public Tag getTagByName(String name) {
        try(Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag where name = :name", Tag.class);
            query.setParameter("name", name);

            List<Tag> result = query.list();
            if(result.size() == 0) {
                throw new EntityNotFoundException("Tag","name", name);
            }

            return result.get(0);
        }
    }

    @Override
    public Set<Post> getTaggedPostsByTagId(int tagId) {
        try (Session session = sessionFactory.openSession()) {
            Tag tag = session.get(Tag.class, tagId);
            if(tag==null){
                throw new EntityNotFoundException("Tag", tagId);
            }

            Set<Post> result = tag.getTaggedPosts();
            return result;
        }
    }

    @Override
    public List<Tag> filter(FilterOptionsTags filterOptionsTags) {
        try(Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from Tag where 1=1");

            if(filterOptionsTags.getName().isPresent()) {
                queryString.append(" and name like :name");
            }

            addSorting(filterOptionsTags, queryString);

            Query<Tag> query = session.createQuery(queryString.toString(), Tag.class);

            if(filterOptionsTags.getName().isPresent()) {
                query.setParameter("name", filterOptionsTags.getName().get());
            }

            return query.list();
        }
    }

    private void addSorting(FilterOptionsTags filterOptions, StringBuilder queryString) {
        if (filterOptions.getSortBy().isEmpty()) {
            return;
        }

        String orderBy = "";

        if(filterOptions.getSortBy().get().equalsIgnoreCase("name")) {
            orderBy = "name";
        }
        queryString.append(String.format(" order by %s", orderBy));

        if (filterOptions.getSortOrder().isPresent() && filterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            queryString.append(" desc");
        }
    }
}
