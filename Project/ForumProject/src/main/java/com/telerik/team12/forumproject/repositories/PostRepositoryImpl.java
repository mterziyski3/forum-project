package com.telerik.team12.forumproject.repositories;

import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.models.*;
import com.telerik.team12.forumproject.repositories.contracts.PostRepository;
import com.telerik.team12.forumproject.repositories.contracts.TagRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public class PostRepositoryImpl implements PostRepository {

    private final SessionFactory sessionFactory;
    private Set<User> likedBy;

    private Set<Reply> replies;
    private final TagRepository tagRepository;

    @Autowired
    public PostRepositoryImpl(SessionFactory sessionFactory, TagRepository tagRepository) {
        this.sessionFactory = sessionFactory;
        this.tagRepository = tagRepository;
    }

    @Override
    public List<Post> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery("from Post", Post.class);
            return query.list();
        }
    }

    public List<Post> getLatestTen() {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery
                    ("from Post where 1=1 order by creationTime desc",
                            Post.class);
            query.setMaxResults(10);
            return query.list();
        }
    }

    public List<Post> getHottestTen() {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery
                    ("from Post where 1=1 order by replies.size desc",
                            Post.class);
            query.setMaxResults(10);
            return query.list();
        }
    }

    public List<Post> getTopTen() {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery
                    ("from Post where 1=1 order by likedBy.size desc",
                            Post.class);
            query.setMaxResults(10);
            return query.list();
        }
    }

    @Override
    public List<Post> filter(FilterOptionsPosts filterOptionsPosts) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from Post where 1=1");

//            if (filterOptionsPosts.getTitle().isPresent()) {
//                queryString.append(" and title like :title");
//            }

            if (filterOptionsPosts.getKeyword().isPresent()) {
                queryString.append(" and title like :keyword or content like :keyword");
            }

            if (filterOptionsPosts.getCategoryId().isPresent()) {
                queryString.append(" and category.id = :categoryId");
            }

            if (filterOptionsPosts.getCreatedByUsername().isPresent()) {
                queryString.append(" and createdBy.username = :createdByUsername");
            }

            if (filterOptionsPosts.getCreationTime().isPresent()) {
                queryString.append(" and creationTime like :creationTime");
            }


            addSorting(filterOptionsPosts, queryString);

            Query<Post> query = session.createQuery(queryString.toString(), Post.class);

//            if (filterOptionsPosts.getTitle().isPresent()) {
//                // HQL: and name like %[some value from user]%
////                query.setParameter("title", String.format
////                        ("%%%s%%", filterOptionsPosts.getTitle().get())); // %some-value%
//                query.setParameter("title", filterOptionsPosts.getTitle().get());
//            }

            if (filterOptionsPosts.getKeyword().isPresent()) {
                query.setParameter("keyword", "%" + filterOptionsPosts.getKeyword().get() + "%");
            }

            if (filterOptionsPosts.getCategoryId().isPresent()) {
                query.setParameter("categoryId", filterOptionsPosts.getCategoryId().get());
            }

            if (filterOptionsPosts.getCreatedByUsername().isPresent()) {
                query.setParameter("createdByUsername", filterOptionsPosts.getCreatedByUsername().get());
            }

            if (filterOptionsPosts.getCreationTime().isPresent()) {
                query.setParameter("creationTime", filterOptionsPosts.getCreationTime().get());
            }


            return query.list();
        }

    }

    private void addSorting(FilterOptionsPosts filterOptionsPosts, StringBuilder queryString) {
        if (filterOptionsPosts.getSortBy().isEmpty()) {
            return;
        }

        String orderBy = "";
        switch (filterOptionsPosts.getSortBy().get()) {
//            case "title":
//                orderBy = "title";
//                break;
            case "createdByUsername":
                orderBy = "createdBy.username";
                break;
            case "category":
                orderBy = "category.name";
                break;
            case "creationTime":
                orderBy = "creationTime";
                break;
            default:
                return;
        }
        queryString.append(String.format(" order by %s", orderBy));


        if (filterOptionsPosts.getSortOrder().isPresent() &&
                filterOptionsPosts.getSortOrder().get().equalsIgnoreCase("desc")) {
            queryString.append(" desc");
        }
    }

    @Override
    public Post getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Post post = session.get(Post.class, id);
            if (post == null) {
                throw new EntityNotFoundException("Post", id);
            }
            return post;
        }
    }

    @Override
    public Post getByTitle(String title) {
        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery("from Post where title = :title", Post.class);
            query.setParameter("title", title);

            List<Post> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Post", "title", title);
            }
            return result.get(0);
        }
    }

    @Override
    public Post createPost(Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.save(post);
        }
        return post;
    }

    @Override
    public void update(Post post) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(post);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        Post postToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(postToDelete);
            session.getTransaction().commit();
        }

    }

    @Override
    public void toggleLike(int id, User user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Post post = getById(id);

            if (post.getLikedBy().contains(user)) {
                post.getLikedBy().remove(user);
                user.getLikedPosts().remove(post);
            } else {
                post.getLikedBy().add(user);
                user.getLikedPosts().add(post);
            }
            session.saveOrUpdate(post);
            session.getTransaction().commit();
        }
    }

    public void toggleTag(int id, Tag tag){
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Post post = getById(id);

            if (post.getPostTags().contains(tag)) {
                post.getPostTags().remove(tag);
                tag.getTaggedPosts().remove(post);
            } else {
                post.getPostTags().add(tag);
                tag.getTaggedPosts().add(post);
            }
            session.saveOrUpdate(post);
            session.saveOrUpdate(tag);
            session.getTransaction().commit();
        }

    }

    @Override
    public List<Post> getPosts() {

        try (Session session = sessionFactory.openSession()) {
            Query<Post> query = session.createQuery("from Post", Post.class);
            return query.list();
        }
    }

    public Set<Reply> getRepliesByPostId(int postId) {
        try (Session session = sessionFactory.openSession()) {
            Post post = session.get(Post.class, postId);
            if (post == null) {
                throw new EntityNotFoundException("Post", postId);
            }
            Set<Reply> result = post.getReplies();
            return result;
        }
    }

    public Set<Tag> getTagsByPostId(int postId) {
        try (Session session = sessionFactory.openSession()) {
            Post post = session.get(Post.class, postId);
            if (post == null) {
                throw new EntityNotFoundException("Post", postId);
            }
            Set<Tag> result = post.getPostTags();
            return result;
        }
    }


}



