package com.telerik.team12.forumproject.services.mappers;


import com.telerik.team12.forumproject.models.Dtos.TagDto;
import com.telerik.team12.forumproject.models.FilterOptionsTags;
import com.telerik.team12.forumproject.models.Tag;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Optional;

@Component
public class TagMapper {

    public Tag fromDto(TagDto tagDto) {
        Tag tag = new Tag();
        tag.setId(tagDto.getId());
        tag.setName(tagDto.getName());

        return tag;
    }

    private void dtoToObject(TagDto tagDto, Tag tag) {
        tag.setName(tagDto.getName());
    }

    public FilterOptionsTags fromDtoDisplay(TagDto tagDto) {
        FilterOptionsTags filterOptionsTags = new FilterOptionsTags(
                Optional.ofNullable(tagDto.getName()),
                Optional.ofNullable(tagDto.getSortBy()),
                Optional.ofNullable(tagDto.getSortOrder())
        );
        return filterOptionsTags;
    }

    public TagDto toDto(Tag tag) {
        TagDto tagDto = new TagDto();
        tagDto.setName(tag.getName());

        return tagDto;
    }
}
