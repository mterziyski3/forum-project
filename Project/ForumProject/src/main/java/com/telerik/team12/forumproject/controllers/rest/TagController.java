package com.telerik.team12.forumproject.controllers.rest;

import com.telerik.team12.forumproject.controllers.AuthenticationHelper;
import com.telerik.team12.forumproject.exceptions.DuplicateEntityException;
import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.exceptions.UnauthorizedOperationException;
import com.telerik.team12.forumproject.models.Dtos.PostDtoOut;
import com.telerik.team12.forumproject.models.Dtos.TagDto;
import com.telerik.team12.forumproject.models.FilterOptionsTags;
import com.telerik.team12.forumproject.models.Post;
import com.telerik.team12.forumproject.models.Tag;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.services.contracts.TagService;
import com.telerik.team12.forumproject.services.mappers.PostMapper;
import com.telerik.team12.forumproject.services.mappers.TagMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/tags")
public class TagController {
    private TagService service;
    private final PostMapper postMapper;

    private final TagMapper tagMapper;
    private AuthenticationHelper authenticationHelper;

    @Autowired
    public TagController(TagService service, PostMapper postMapper,
                         TagMapper tagMapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.postMapper = postMapper;
        this.tagMapper = tagMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/filter")
    public List<Tag> filter(
            @RequestParam(required = false) Optional<String> name,
            @RequestParam(required = false) Optional<String> sortBy,
            @RequestParam(required = false) Optional<String> sortOrder) {
        return service.filter(new FilterOptionsTags(name, sortBy, sortOrder));
    }

    @GetMapping
    public List<Tag> getTags() {
        return service.getTags();
    }


    @GetMapping("/taggedPosts")
    public List<PostDtoOut> getTaggedPosts(@RequestHeader HttpHeaders headers,
                                           @RequestParam(required = true) int tagId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            List<Post> result = new ArrayList<>(service.getTaggedPostsByTagId(tagId, user));
            return result.stream().map(postMapper::objectToDtoOut)
                    .collect(Collectors.toList());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Tag getTag(@PathVariable @Valid int id) {
        try {
            return service.getTag(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @PostMapping
    public Tag createTag(@Valid @RequestBody TagDto tagDto) {
        try {
            Tag tag = tagMapper.fromDto(tagDto);
            service.createTag(tag);
            return tag;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Tag updateTag(@PathVariable int id, @RequestBody @Valid Tag tag) {
        try {
            Tag tagToUpdate = service.getTag(tag.getId());
            service.updateTag(tag);

            return tag;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public Tag deleteTag(@PathVariable int id) {
        try {
            return service.deleteTag(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }
}
