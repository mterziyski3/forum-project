package com.telerik.team12.forumproject.services.contracts;

import com.telerik.team12.forumproject.models.*;

import java.util.List;
import java.util.Set;

public interface PostService {

    List<Post> getAllForAdmins(User user);

    List<Post> filter(FilterOptionsPosts filterOptionsPosts, User user);
    List<Post>getLatestTen();
    List<Post>getHottestTen();
    List<Post> getTopTen();

    Post getById(int id, User user);

    Post getByTitle(String name, User user);

    Post createPost(Post post, User user);

    void update(Post post, User user);

    void delete(int id, User user);

    void toggleLike(int id, User user);

    Post getById(int id);

    List<Post> getPosts();

    Set<Reply> getRepliesByPostId(int postId);
    Set<Tag> getTagsByPostId(int postId);
    void addTag(int postId, Tag tag, User user);

    void toggleTag(int id, Tag tag);
}
