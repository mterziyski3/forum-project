package com.telerik.team12.forumproject;

import com.telerik.team12.forumproject.controllers.mvc.FileUploadController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import java.io.File;

@SpringBootApplication
public class ForumProjectApplication {

	public static void main(String[] args) {
		new File(FileUploadController.uploadDirectory).mkdir();
		SpringApplication.run(ForumProjectApplication.class, args);
	}

}
