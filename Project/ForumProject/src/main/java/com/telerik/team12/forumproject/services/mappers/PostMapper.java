package com.telerik.team12.forumproject.services.mappers;

import com.telerik.team12.forumproject.models.*;
import com.telerik.team12.forumproject.models.Dtos.PostDto;
import com.telerik.team12.forumproject.models.Dtos.PostDtoAdvancedView;
import com.telerik.team12.forumproject.models.Dtos.PostDtoOut;
import com.telerik.team12.forumproject.models.Dtos.PostFilterDto;
import com.telerik.team12.forumproject.services.contracts.CategoryService;
import com.telerik.team12.forumproject.services.contracts.PostService;
import com.telerik.team12.forumproject.services.contracts.UserService;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class PostMapper {

    private final CategoryService categoryService;
    private final UserService userService;
    private final PostService postService;
    private final ReplyMapper replyMapper;

    private final TagMapper tagMapper;

    public PostMapper(CategoryService categoryService, UserService userService, PostService postService, ReplyMapper replyMapper, TagMapper tagMapper) {
        this.categoryService = categoryService;
        this.userService = userService;
        this.postService = postService;
        this.replyMapper = replyMapper;
        this.tagMapper = tagMapper;
    }

    public Post fromDto(PostDto postDTO) {
        Post post = new Post();
        dtoToObject(postDTO, post);
        post.setCreationTime(LocalDateTime.now());
        return post;
    }

    public Post fromDto(PostDto postDto, int id) {
        Post post = postService.getById(id);
        dtoToObject(postDto, post);
        return post;
    }

    public void dtoToObject(PostDto postDto, Post post){
       User user = userService.getUser(postDto.getCreatedById());
       Category category = categoryService.getById(postDto.getCategoryId());
        post.setTitle(postDto.getTitle());
        post.setContent(postDto.getContent());
        post.setCategory(category);
        post.setCreatedBy(user);

    }

    public PostDto objectToDTO(Post post) {
        PostDto postDTO = new PostDto();
        postDTO.setTitle(post.getTitle());
        postDTO.setContent(post.getContent());
        postDTO.setCategoryId(post.getCategory().getId());
        postDTO.setCreatedById(post.getCreatedBy().getId());
     //   postDTO.setCreationTime(post.getCreationTime());
        return postDTO;
    }


    public PostDtoOut objectToDtoOut(Post post){
        PostDtoOut postDTOOut = new PostDtoOut();
        postDTOOut.setTitle(post.getTitle());
        postDTOOut.setContent(post.getContent());
        postDTOOut.setCategoryName(post.getCategory().getName());
        postDTOOut.setCreatedByUsername(post.getCreatedBy().getUsername());
        postDTOOut.setCreatedByFullName(post.getCreatedBy().getFirstName() + " "
                + post.getCreatedBy().getLastName());
        postDTOOut.setRepliesCount(post.getReplies().size());
        postDTOOut.setLikesCount(post.getLikedBy().size());
        postDTOOut.setCreationTime(post.getCreationTime());
        return postDTOOut;
    }

    public PostDtoAdvancedView objectToDtoAdvancedView(Post post){
        PostDtoAdvancedView postDtoAdvancedView = new PostDtoAdvancedView();
        postDtoAdvancedView.setPostId(post.getId());
        postDtoAdvancedView.setTitle(post.getTitle());
        postDtoAdvancedView.setContent(post.getContent());
        postDtoAdvancedView.setCategoryName(post.getCategory().getName());
        postDtoAdvancedView.setCreatedByUsername(post.getCreatedBy().getUsername());
        postDtoAdvancedView.setCreatedByFullName(post.getCreatedBy().getFirstName() + " "
                + post.getCreatedBy().getLastName());
        postDtoAdvancedView.setCreationTime(post.getCreationTime());
        postDtoAdvancedView.setLikesCount(post.getLikedBy().size());
        List<Reply> postReplies = new ArrayList<>(postService.getRepliesByPostId(post.getId()));
        postDtoAdvancedView.setReplies(postReplies.stream()
                .map(replyMapper:: objectToDTOForNormalUser)
                .collect(Collectors.toList()));
        List<Tag> postTags = new ArrayList<>(postService.getTagsByPostId(post.getId()));
        postDtoAdvancedView.setPostTags(postTags.stream()
                .map(tagMapper::toDto)
                .collect(Collectors.toList()));
        return postDtoAdvancedView;
    }

    public FilterOptionsPosts fromDto(PostFilterDto postFilterDto) {

        FilterOptionsPosts filterOptionsPosts = new FilterOptionsPosts(
//                Optional.ofNullable(postFilterDto.getTitle()),
                Optional.ofNullable(postFilterDto.getKeyword()),
                Optional.ofNullable(postFilterDto.getCategoryId()),
                Optional.ofNullable(postFilterDto.getCreatedByUsername()),
                Optional.ofNullable(postFilterDto.getCreationTime()),
                Optional.ofNullable(postFilterDto.getSortBy()),
                Optional.ofNullable(postFilterDto.getSortOrder())
        );

        return filterOptionsPosts;
    }
}
