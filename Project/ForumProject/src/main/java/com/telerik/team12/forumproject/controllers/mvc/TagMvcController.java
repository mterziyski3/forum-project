package com.telerik.team12.forumproject.controllers.mvc;


import com.telerik.team12.forumproject.controllers.AuthenticationHelper;
import com.telerik.team12.forumproject.exceptions.AuthenticationFailureException;
import com.telerik.team12.forumproject.exceptions.DuplicateEntityException;
import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.exceptions.UnauthorizedOperationException;
import com.telerik.team12.forumproject.models.Dtos.TagDto;
import com.telerik.team12.forumproject.models.Dtos.UserDto;
import com.telerik.team12.forumproject.models.FilterOptionsTags;
import com.telerik.team12.forumproject.models.Tag;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.services.contracts.TagService;
import com.telerik.team12.forumproject.services.mappers.TagMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/tags")
public class TagMvcController {

    private final TagService tagService;
    private final TagMapper tagMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public TagMvcController(TagService tagService, TagMapper tagMapper, AuthenticationHelper authenticationHelper) {
        this.tagService = tagService;
        this.tagMapper = tagMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("IsAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @GetMapping
    public String showAllTags(@ModelAttribute("filterOptions") TagDto tagDto, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect/auth/login";
        }
        FilterOptionsTags filterOptionsTags = tagMapper.fromDtoDisplay(tagDto);
        List<Tag> tags = tagService.filter(filterOptionsTags);
        model.addAttribute("tags", tags);
        model.addAttribute("filterOptions", tagDto);
        return "TagsView";

    }

    @GetMapping("/{id}")
    public String showSingleTag(@PathVariable int id, Model model) {
        try {
            Tag tag = tagService.getTag(id);
            model.addAttribute("tag", tag);
            return "TagView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/new")
    public String showNewTagPage(Model model, HttpSession httpSession) {
        try {
            authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("tag", new TagDto());

        return "Tag-New";
    }

    @PostMapping("/new")
    public String createTag(@Valid @ModelAttribute("tag") TagDto tagDto, BindingResult bindingResult, Model model, HttpSession httpSession) {
        User userWhoIsUpdating;
        try {
            userWhoIsUpdating = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "Tag-New";
        }

        try {
            Tag tag = tagMapper.fromDto(tagDto);
            tagService.createTag(tag);

            return "redirect:/tags";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("name", "duplicateTagName", e.getMessage());

            return "Tag-New";
        }
    }

    @GetMapping("/{id}/edit")
    public String showEditTagPage(@PathVariable int id, Model model, HttpSession httpSession) {
        User userWhoIsUpdating;
        try {
            userWhoIsUpdating = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (!userWhoIsUpdating.checkIfUserAdmin()) {
            return "Access-Denied";
        }

        try {
            Tag tag = tagService.getTag(id);
            TagDto tagDto = tagMapper.toDto(tag);
            model.addAttribute("tag", tagDto);

            return "Tag-Update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{id}/edit")
    public String editTag(@PathVariable int id, @Valid @ModelAttribute("tag") TagDto tagDto, BindingResult bindingResult, Model model, HttpSession httpSession) {
        User userWhoIsUpdating;
        try {
            userWhoIsUpdating = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (bindingResult.hasErrors()) {
            return "User-Update";
        }

        if (!userWhoIsUpdating.checkIfUserAdmin()) {
            return "Access-Denied";
        }

        try {
            Tag tag = tagMapper.fromDto(tagDto);
            tagService.updateTag(tag);

            return "redirect:/tags";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("name", "duplicateTag", e.getMessage());

            return "Tag-Update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";

        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());

            return "Access-Denied";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteTag(@PathVariable int id, Model model, HttpSession httpSession) {
        User userWhoIsUpdating;
        try {
            userWhoIsUpdating = authenticationHelper.tryGetUser(httpSession);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (!userWhoIsUpdating.checkIfUserAdmin()) {
            return "Access-Denied";
        }

        try {
            tagService.deleteTag(id);

            return "redirect:/tags";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "Access-Denied";

        }
    }
}
