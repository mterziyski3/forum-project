package com.telerik.team12.forumproject.models.Dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CategoryDto {

    @NotNull
    @Size(min = 2, max = 50, message = "Category name should be between 2 and 50 symbols")
    private String name;

    private int id;

    public CategoryDto() {

    }

    public CategoryDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId(int id) {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
