package com.telerik.team12.forumproject.services.mappers;

import com.telerik.team12.forumproject.models.Category;
import com.telerik.team12.forumproject.models.Dtos.CategoryDto;
import com.telerik.team12.forumproject.models.Dtos.CategoryDtoOut;
import com.telerik.team12.forumproject.models.Dtos.PostDtoOutForCategory;
import com.telerik.team12.forumproject.models.FilterOptionsCategory;
import com.telerik.team12.forumproject.services.contracts.CategoryService;
import com.telerik.team12.forumproject.services.contracts.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class CategoryMapper {

    private final CategoryService categoryService;
    private final PostService postService;

    @Autowired
    public CategoryMapper(CategoryService categoryService, PostService postService) {
        this.categoryService = categoryService;
        this.postService = postService;
    }

    public Category dtoToObject(CategoryDto categoryDTO, Category category) {
        category.setName(categoryDTO.getName());
        return category;
    }

    public CategoryDtoOut objectToDTO(Category category) {
        CategoryDtoOut categoryDTOout = new CategoryDtoOut();
        categoryDTOout.setName(category.getName());
        return categoryDTOout;
    }

    public CategoryDtoOut toDisplayDto(Category category) {
        CategoryDtoOut categoryDtoOut = new CategoryDtoOut();

        categoryDtoOut.setId(category.getId());
        categoryDtoOut.setName(category.getName());
        categoryDtoOut.setPostCount(category.getPosts().size());

        Set<PostDtoOutForCategory> posts = new HashSet<>();
        category.getPosts().forEach(post -> posts.add(new PostDtoOutForCategory(post)));
        categoryDtoOut.setPosts(posts);

        return categoryDtoOut;
    }

    public FilterOptionsCategory fromDto(CategoryDtoOut categoryDtoOut) {

        FilterOptionsCategory filterOptionsCategory = new FilterOptionsCategory(
                Optional.ofNullable(categoryDtoOut.getName()),
                Optional.ofNullable(categoryDtoOut.getSortBy()),
                Optional.ofNullable(categoryDtoOut.getSortOrder())
        );
        return filterOptionsCategory;
    }



}
