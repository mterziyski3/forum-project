package com.telerik.team12.forumproject.models;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.Optional;

public class FilterOptionsPosts {

//    private Optional<String> title;

    private Optional<String> keyword;

    private Optional<Integer> categoryId;

    private Optional<String> createdByUsername;

    private Optional<LocalDateTime>creationTime;

    private Optional<String> sortBy;
    private Optional<String> sortOrder;

    public FilterOptionsPosts(
                              Optional<String> keyword,
                              Optional<Integer> categoryId,
                              Optional<String> createdByUsername,
                              Optional<LocalDateTime> creationTime,
                              Optional<String> sortBy,
                              Optional<String> sortOrder) {
//        this.title = title;
        this.keyword = keyword;
        this.categoryId = categoryId;
        this.createdByUsername = createdByUsername;
        this.creationTime = creationTime;
        this.sortBy = sortBy;
        this.sortOrder = sortOrder;
    }


//    public Optional<String> getTitle() {
//        return title;
//    }

    public Optional<Integer> getCategoryId() {
        return categoryId;
    }

    public Optional<String> getCreatedByUsername() {
        return createdByUsername;
    }

    public Optional<LocalDateTime> getCreationTime() {
        return creationTime;
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }

    public Optional<String> getKeyword() {
        return keyword;
    }
}
