package com.telerik.team12.forumproject.controllers.mvc;

import com.telerik.team12.forumproject.controllers.AuthenticationHelper;
import com.telerik.team12.forumproject.exceptions.AuthenticationFailureException;
import com.telerik.team12.forumproject.exceptions.DuplicateEntityException;
import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.exceptions.UnauthorizedOperationException;
import com.telerik.team12.forumproject.models.Category;
import com.telerik.team12.forumproject.models.Dtos.CategoryDto;
import com.telerik.team12.forumproject.models.Dtos.CategoryDtoOut;
import com.telerik.team12.forumproject.models.FilterOptionsCategory;
import com.telerik.team12.forumproject.models.Post;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.services.contracts.CategoryService;
import com.telerik.team12.forumproject.services.contracts.PostService;
import com.telerik.team12.forumproject.services.contracts.UserService;
import com.telerik.team12.forumproject.services.mappers.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/categories")
public class CategoryMvcController {

    private final CategoryService categoryService;
    private final UserService userService;
    private final CategoryMapper categoryMapper;

    private final PostService postService;

    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CategoryMvcController(CategoryService categoryService, UserService userService, CategoryMapper categoryMapper, PostService postService, AuthenticationHelper authenticationHelper) {
        this.categoryService = categoryService;
        this.userService = userService;
        this.categoryMapper = categoryMapper;
        this.postService = postService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("IsAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("posts")
    public List<Post> populatePosts() {
            return postService.getPosts();
    }

    @GetMapping
    public String showAllCategories(@ModelAttribute("filterOptions") CategoryDtoOut categoryDtoOut, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect/auth/login";
        }
        FilterOptionsCategory filterOptionsCategory = categoryMapper.fromDto(categoryDtoOut);
        List<CategoryDtoOut> categories = categoryService.filter(filterOptionsCategory)
                .stream()
                .map(categoryMapper::toDisplayDto)
                .collect(Collectors.toList());

        //  model.addAttribute("post", )
        // model.addAttribute("posts", categories.stream().map(post -> postService.getPosts()));
//        model.addAttribute("category", categories.stream().map(CategoryDtoOut::getId));

        model.addAttribute("categories", categories);
        model.addAttribute("filterOptions", categoryDtoOut);
        return "CategoriesView";
    }




    @GetMapping("/{id}")
    public String showSingleCategory(@PathVariable int id, Model model) {
        try {
            Category category = categoryService.getById(id);
            model.addAttribute("category", category);
            return "CategoryView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/new")
    public String showNewCategoryPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect/auth/login";
        }
        model.addAttribute("category", new CategoryDto());
        return "Category-New";
    }

    @PostMapping("/new")
    public String createCategory(@Valid @ModelAttribute("category") CategoryDto categoryDto,
                                 Model model,
                                 BindingResult errors,
                                 HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect/auth/login";
        }
        if (errors.hasErrors()) {
            return "Category-New";
        }
        try {
            Category newCategory = categoryMapper.dtoToObject(categoryDto, new Category());
            categoryService.create(newCategory, user);
            return "redirect:/categories";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "category.exists", e.getMessage());
            return "Category-New";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "Access-Denied";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditCategoryPage(@PathVariable int id, Model model, HttpSession session) {
       try {
           authenticationHelper.tryGetUser(session);
       } catch (AuthenticationFailureException e) {
           return "redirect:/replies";
       }
        Category category = categoryService.getById(id);
        CategoryDtoOut categoryDtoOut = categoryMapper.objectToDTO(category);
        model.addAttribute("categoryId", id);
        model.addAttribute("category", categoryDtoOut);
        return "Category-update";
    }

    @PostMapping("/{id}/update")
    public String updateCategory(@PathVariable int id, @Valid @ModelAttribute("category") CategoryDto categoryDto,
                                 Model model,
                                 BindingResult errors,
                                 HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect/auth/login";
        }
        if (errors.hasErrors()) {
            return "Category-update";
        }
        try {
            Category categoryToUpdate = categoryService.getById(categoryDto.getId(id));
            Category category = categoryMapper.dtoToObject(categoryDto, categoryToUpdate);

            categoryService.update(category, user);
            return "redirect:/categories";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "category.exists", e.getMessage());
            return "Category-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "Access-Denied";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteCategory(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect/auth/login";
        }
        try {
            categoryService.delete(id, user);
            return "redirect:/categories";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "Access-Denied";
        }
    }
}



