package com.telerik.team12.forumproject.services;

import com.telerik.team12.forumproject.exceptions.DuplicateEntityException;
import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.exceptions.UnauthorizedOperationException;
import com.telerik.team12.forumproject.models.Category;
import com.telerik.team12.forumproject.models.FilterOptionsCategory;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.repositories.contracts.CategoryRepository;
import com.telerik.team12.forumproject.services.contracts.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private static final String CREATE_CATEGORY_ERROR_MESSAGE = "Only admin can create a category.";

    private static final String GET_ALL_REPLY_ERROR_MESSAGE = "Only admin can list all categories.";
    private static final String MODIFY_CATEGORY_ERROR_MESSAGE = "Only admin can modify a category.";
    private static final String DELETE_CATEGORY_ERROR_MESSAGE = "Only admin can delete a category.";
    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository)    {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAllCategories() {
//        if (!user.checkIfUserAdmin()) {
//            throw new UnauthorizedOperationException(GET_ALL_REPLY_ERROR_MESSAGE);
//        }
        return categoryRepository.getAllCategories();
    }

    @Override
    public List<Category> filter(FilterOptionsCategory filterOptionsCategory) {
        return categoryRepository.filter(filterOptionsCategory);
    }

    @Override
    public Category getById(int id) {
        return categoryRepository.getById(id);
    }

    @Override
    public Category create(Category category, User user) {
        if (!user.checkIfUserAdmin()) {
            throw new UnauthorizedOperationException(CREATE_CATEGORY_ERROR_MESSAGE);
        }
        boolean duplicateExists = true;
        try {
            categoryRepository.getByName(category.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Category", "name", category.getName());
        }

        categoryRepository.create(category);
        return category;
    }

    @Override
    public Category update(Category category, User user) {
        if (!user.checkIfUserAdmin()) {
            throw new UnauthorizedOperationException(MODIFY_CATEGORY_ERROR_MESSAGE);
        }

        boolean duplicateExists = true;
        try {
            Category existingCategory = categoryRepository.getByName(category.getName());
            if (existingCategory.getId() == category.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Category", "name", category.getName());
        }

        categoryRepository.update(category);
        return category;
    }

    @Override
    public void delete(int id, User user) {
        if (!user.checkIfUserAdmin()) {
            throw new UnauthorizedOperationException(DELETE_CATEGORY_ERROR_MESSAGE);
        }
        categoryRepository.delete(id);
    }

//    public Post addPostByTitleToCategory(int id, Post postToAdd, User user) {
//        Category category = categoryRepository.getById(id);
//        Post post = postService.createPost(postToAdd, user);
//
//        category.getPosts().add(post);
//        categoryRepository.update(category);
//
//        return post;
//    }
}
