package com.telerik.team12.forumproject.services.contracts;

import com.telerik.team12.forumproject.models.Category;
import com.telerik.team12.forumproject.models.FilterOptionsCategory;
import com.telerik.team12.forumproject.models.User;

import java.util.List;

public interface CategoryService {

     List<Category> getAllCategories();

     List<Category> filter(FilterOptionsCategory filterOptionsCategory);

     Category getById(int id);

     Category create(Category category, User user);

     Category update(Category category, User user);

     void delete(int id, User user);
}
