package com.telerik.team12.forumproject.services.mappers;

import com.telerik.team12.forumproject.models.Dtos.ReplyDTO;
import com.telerik.team12.forumproject.models.Dtos.ReplyDtoOut;
import com.telerik.team12.forumproject.models.Dtos.ReplyDtoOutNormalUser;
import com.telerik.team12.forumproject.models.FilterOptionsReplies;
import com.telerik.team12.forumproject.models.Post;
import com.telerik.team12.forumproject.models.Reply;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.services.contracts.PostService;
import com.telerik.team12.forumproject.services.contracts.ReplyService;
import com.telerik.team12.forumproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;


@Component
public class ReplyMapper {

    private final ReplyService replyService;

    private final PostService postService;

    private final UserService userService;



    @Autowired
    public ReplyMapper(ReplyService replyService, PostService postService, UserService userService) {
        this.replyService = replyService;
        this.postService = postService;
        this.userService = userService;
    }

    public Reply dtoToObject(User user, Post post, ReplyDTO replyDTO) {
//        User user = userService.getUser(replyDTO.getUser_id());
//        Post post = postService.getById(replyDTO.getPost_id(), user);
//        reply.setContent(replyDTO.getContent());
//        reply.setCreationTime(replyDTO.getCreationTime());
//        reply.setCreatedBy(user);
//        reply.setPost(post);

        Reply reply = new Reply();

        reply.setContent(replyDTO.getContent());
        reply.setCreationTime(replyDTO.getCreationTime());
        reply.setCreatedBy(user);
        reply.setPost(post);

        return reply;
    }

    public Reply dtoToReply(ReplyDTO replyDTO,Reply reply){
//        User user = userService.getByUsername(commentDTO.getAuthor());
        reply.setContent(replyDTO.getContent());
        reply.setCreationTime(replyDTO.getCreationTime());
//        comment.setAuthor(user);
        return reply;
    }
//    public Reply dtoToObject(ReplyDTO replyDTO,User user) {
//        Reply reply = new Reply();
//   // Post post = postService.getById(replyDTO.getPost_id(), user);
//    reply.setContent(replyDTO.getContent());
//    reply.setCreationTime(replyDTO.getCreationTime());
//    reply.setCreatedBy(user);
//   // reply.setPost(post);
//
//    return reply;
//}

    public ReplyDtoOut objectToDTO(Reply reply) {
        ReplyDtoOut replyDTOOut = new ReplyDtoOut();
        replyDTOOut.setContent(reply.getContent());
//        replyDTOOut.setCreatedBy(reply.getCreatedBy());
        replyDTOOut.setCreationTime(reply.getCreationTime());
        replyDTOOut.setPostTitle(reply.getPost().getTitle());
        replyDTOOut.setPostId(reply.getPost().getId());

        User user = new User();
        user.setUsername(user.getUsername());
        reply.setCreatedBy(user);

        Post post = new Post();
        post.setTitle(post.getTitle());
        post.setId(post.getId());
        reply.setPost(post);

        return replyDTOOut;
    }

    public ReplyDtoOutNormalUser objectToDTOForNormalUser(Reply reply) {
        ReplyDtoOutNormalUser replyDTOoutNormalUser = new ReplyDtoOutNormalUser();
        replyDTOoutNormalUser.setContent(reply.getContent());
        replyDTOoutNormalUser.setCreationTime(reply.getCreationTime());
        replyDTOoutNormalUser.setUser(reply.getCreatedBy().getUsername());
        replyDTOoutNormalUser.setPostTitle(reply.getPost().getTitle());


//        User user = new User(getUsername());
//        user.setUsername(user.getUsername());
//        reply.setCreatedBy(user);
        User user = new User();
        user.setUsername(user.getUsername());
        reply.setCreatedBy(user);

        Post post = new Post();
        post.setTitle(post.getTitle());
        reply.setPost(post);

        return replyDTOoutNormalUser;
    }

    public ReplyDtoOut toDisplayDto(Reply reply) {
        ReplyDtoOut replyDtoOut = new ReplyDtoOut();

        replyDtoOut.setId(reply.getId());
        replyDtoOut.setContent(reply.getContent());
        replyDtoOut.setCreatedBy(reply.getCreatedBy().getUsername());
        replyDtoOut.setCreationTime(reply.getCreationTime());

        return replyDtoOut;
    }

    public FilterOptionsReplies fromDto(ReplyDtoOut replyDtoOut) {

        FilterOptionsReplies filterOptionsReplies = new FilterOptionsReplies(
                Optional.ofNullable(replyDtoOut.getContent()),
                Optional.ofNullable(replyDtoOut.getCreatedBy()),
                Optional.ofNullable(replyDtoOut.getCreationTime()),
                Optional.ofNullable(replyDtoOut.getSortBy()),
                Optional.ofNullable(replyDtoOut.getSortOrder())
        );
        return filterOptionsReplies;
    }
}
