package com.telerik.team12.forumproject.controllers.mvc;

import com.telerik.team12.forumproject.controllers.AuthenticationHelper;
import com.telerik.team12.forumproject.exceptions.AuthenticationFailureException;
import com.telerik.team12.forumproject.exceptions.DuplicateEntityException;
import com.telerik.team12.forumproject.models.Dtos.LoginDto;
import com.telerik.team12.forumproject.models.Dtos.RegisterDto;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.services.contracts.UserService;
import com.telerik.team12.forumproject.services.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/auth")
public class AuthenticationController {
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;

    @Autowired
    public AuthenticationController(UserService userService,
                                    AuthenticationHelper authenticationHelper,
                                    UserMapper userMapper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model) {
        model.addAttribute("login", new LoginDto());
        return "Login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto loginDto, BindingResult bindingResult, HttpSession httpSession) {
        if(bindingResult.hasErrors()) {
            return "login";
        }

        try {
            authenticationHelper.verifyAuthentication(loginDto.getUsername(), loginDto.getPassword());
            httpSession.setAttribute("currentUser", loginDto.getUsername());

            return "redirect:/";
        } catch(AuthenticationFailureException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());

            return "Login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession httpSession) {
        httpSession.removeAttribute("currentUser");
        return "redirect:/";
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDto());

        return "Register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("login") RegisterDto registerDto, BindingResult bindingResult, HttpSession httpSession) {
        if(bindingResult.hasErrors()) {
            return "Register";
        }

        if (!registerDto.getPassword().equals(registerDto.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error", "Password confirmation should match password.");
            return  "Register";
        }

        try {
            User user = userMapper.fromRegisterDto(registerDto);
            userService.createUser(user);

            return "redirect:/auth/login";
        } catch(DuplicateEntityException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());

            return "Register";
        }
    }
}
