package com.telerik.team12.forumproject.controllers.mvc;

import com.telerik.team12.forumproject.controllers.AuthenticationHelper;
import com.telerik.team12.forumproject.exceptions.AuthenticationFailureException;
import com.telerik.team12.forumproject.exceptions.DuplicateEntityException;
import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.exceptions.UnauthorizedOperationException;
import com.telerik.team12.forumproject.models.*;
import com.telerik.team12.forumproject.models.Dtos.*;
import com.telerik.team12.forumproject.services.contracts.CategoryService;
import com.telerik.team12.forumproject.services.contracts.PostService;
import com.telerik.team12.forumproject.services.contracts.TagService;
import com.telerik.team12.forumproject.services.contracts.UserService;
import com.telerik.team12.forumproject.services.mappers.PostMapper;
import com.telerik.team12.forumproject.services.mappers.TagMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/posts")
public class PostMvcController {

    private final PostService postService;
    private final CategoryService categoryService;
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final PostMapper postMapper;
    private final TagMapper tagMapper;
    private final TagService tagService;

    @Autowired
    public PostMvcController(PostService postService,
                             CategoryService categoryService,
                             AuthenticationHelper authenticationHelper,
                             UserService userService,
                             PostMapper postMapper,
                             TagMapper tagMapper,
                             TagService tagService) {
        this.postService = postService;
        this.categoryService = categoryService;
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.postMapper = postMapper;
        this.tagMapper = tagMapper;
        this.tagService = tagService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAllCategories();
    }

    @ModelAttribute("tags")
    public List<Tag> populateTags() {
        return tagService.getTags();
    }

    @ModelAttribute("users")
    public List<User> populateUsers() {
        return userService.getUsers();
    }

    @GetMapping
    public String filterPosts(@ModelAttribute("filterOptionsPosts") PostFilterDto postFilterDto,
                              Model model,
                              HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        FilterOptionsPosts filterOptionsPosts = postMapper.fromDto(postFilterDto);
        List<Post> posts = postService.filter(filterOptionsPosts, user);

        model.addAttribute("filterOptionsPosts", postFilterDto);
        model.addAttribute("posts", posts);
        return "posts-view";
    }

    @GetMapping("/{id}")
    public String showSinglePost(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Post post = postService.getById(id);
            PostDtoAdvancedView postDtoAdvancedView = postMapper.objectToDtoAdvancedView(post);
            model.addAttribute("post", postDtoAdvancedView);
            model.addAttribute("reply", new ReplyDTO());
            model.addAttribute("tag", new TagDto());
            model.addAttribute("currentUser", userService.getById(user.getId()));
            return "post-view";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @GetMapping("/latest")
    public String showLatestTentPosts(HttpSession httpSession, Model model) {
        List<Post> latestTen = postService.getLatestTen();
        latestTen.stream().map(postMapper::objectToDtoOut)
                .collect(Collectors.toList());
        model.addAttribute("latestTen", postService.getLatestTen());
        model.addAttribute("posts", latestTen);
        return "IndexView";
    }


    @GetMapping("/hottest")
    public String showHottestTentPosts(HttpSession httpSession, Model model) {
        List<Post> hottestTen = postService.getHottestTen();
        hottestTen.stream().map(postMapper::objectToDtoOut)
                .collect(Collectors.toList());
        model.addAttribute("hottestTen", postService.getHottestTen());
        model.addAttribute("posts", hottestTen);
        return "IndexView";
    }

    @GetMapping("/mostLiked")
    public String showMostLikedTentPosts(HttpSession httpSession, Model model) {
        List<Post> topTen = postService.getTopTen();
        topTen.stream().map(postMapper::objectToDtoOut)
                .collect(Collectors.toList());
        model.addAttribute("topTen", postService.getTopTen());
        model.addAttribute("posts", topTen);
        return "IndexView";
    }

    @GetMapping("/new")
    public String showNewPostPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("post", new PostDto());
        return "post-new";
    }

    @PostMapping("/new")
    public String createPost(@Valid @ModelAttribute("post") PostDto postDto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "post-new";
        }

        try {
            Post post = postMapper.fromDto(postDto);
            postService.createPost(post, user);

            return "redirect:/posts";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("title", "duplicate_post", e.getMessage());
            return "post-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditPostPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Post post = postService.getById(id);
            PostDto postDto = postMapper.objectToDTO(post);
            model.addAttribute("postId", id);
            model.addAttribute("post", postDto);
            return "post-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/update")
    public String updatePost(@PathVariable int id,
                             @Valid @ModelAttribute("post") PostDto dto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "post-update";
        }

        try {
            Post post = postMapper.fromDto(dto, id);
            postService.update(post, user);
            return "redirect:/posts";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_post", e.getMessage());
            return "post-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/updateTag")
    public String showUpdateTagPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            Post post = postService.getById(id);
            PostDto postDto = postMapper.objectToDTO(post);
            model.addAttribute("postId", id);
            model.addAttribute("post", postDto);
            return "post-update-tag";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        }
    }

    @PostMapping("/{id}/updateTag")
    public String updateTag(@PathVariable int id,
                         @Valid @ModelAttribute("tag") TagDto tagDto,
                         BindingResult errors,
                         Model model,
                         HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "post-update-tag";
        }
        try {
            Tag tag = tagMapper.fromDto(tagDto);
            Post post = postService.getById(id);
            model.addAttribute("postId", id);
            model.addAttribute("tag", tag);
            postService.toggleTag(id, tag);
            model.addAttribute("postTags", post.getPostTags());
            model.addAttribute("taggedPosts", tag.getTaggedPosts());
            return "redirect:/posts";
        } catch (EntityNotFoundException e) {
            return "redirect:/tags/new";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @PostMapping("/{id}/addTag")
    public String addTag(@PathVariable int id,
                             @Valid @ModelAttribute("tag") TagDto tagDto,
                             BindingResult errors,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        if (errors.hasErrors()) {
            return "post-update-tag";
        }
        try {
            Tag tag = tagMapper.fromDto(tagDto);
            Post post = postService.getById(id);
            model.addAttribute("postId", id);
            model.addAttribute("tag", tag);
            postService.addTag(id, tag, user);
            model.addAttribute("postTags", post.getPostTags());
            model.addAttribute("taggedPosts", tag.getTaggedPosts());
            return "redirect:/posts";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("name", "duplicate_tag", e.getMessage());
            return "redirect:/posts";
        } catch (EntityNotFoundException e) {
            return "redirect:/tags/new";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

//    @PutMapping("/addTag/{id}")
//    public void addTag(@RequestHeader HttpHeaders headers,
//                       @PathVariable int id, @Valid @RequestBody TagDto tagDto) {
//        try {
//            User user = authenticationHelper.tryGetUser(headers);
//            Tag tag = tagMapper.fromDto(tagDto);
//            postService.addTag(id, tag, user);
//        } catch (EntityNotFoundException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        } catch (DuplicateEntityException e) {
//            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
//        } catch (UnauthorizedOperationException e) {
//            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
//        }
//    }

    @GetMapping("/{id}/delete")
    public String deletePost(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        try {
            postService.delete(id, user);

            return "redirect:/posts";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/toggle-like")
    public String toggleLike(@PathVariable int id, HttpSession httpSession, Model model) {

        try {
            User user = authenticationHelper.tryGetUser(httpSession);
            postService.toggleLike(id, user);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "Access-Denied";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        return String.format("redirect:/posts/%d", id);
    }
}
