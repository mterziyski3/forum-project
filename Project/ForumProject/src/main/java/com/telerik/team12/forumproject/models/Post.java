package com.telerik.team12.forumproject.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "posts")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id")
    private int id;
    @Column(name = "title")
    private String title;
    @Column(name = "content")
    private String content;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User createdBy;
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "creation_time")
    private LocalDateTime creationTime;

    @ManyToMany(fetch = FetchType.EAGER)
   @JoinTable(name = "user_likes", joinColumns = @JoinColumn(name = "post_id"),
           inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Set<User> likedBy;


    @OneToMany(mappedBy = "post", fetch = FetchType.EAGER)
      private Set<Reply> replies;

      @ManyToMany(fetch = FetchType.EAGER)
      @JoinTable(name = "posts_tags", joinColumns = @JoinColumn(name = "post_id"),
           inverseJoinColumns = @JoinColumn(name = "tag_id"))
      private Set<Tag> postTags;

    public Post() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

      public Set<Reply> getReplies() {
      return replies;
 }
  public void setReplies(Set<Reply> replies) {
     this.replies = replies;
    }

    public Set<User> getLikedBy() {
        return likedBy;
    }

    public void setLikedBy(Set<User> likedBy) {
        this.likedBy = likedBy;
    }

   public Set<Tag> getPostTags() {
       return postTags;
    }

    public void setPostTags(Set<Tag> postTags) {
       this.postTags = postTags;
    }

    public LocalDateTime getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(LocalDateTime creationTime) {
        this.creationTime = creationTime;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Post)) return false;
        Post post = (Post) o;
        return getId() == post.getId() &&
                Objects.equals(getTitle(), post.getTitle()) && Objects.equals(getContent(),
                post.getContent()) && Objects.equals(getCreatedBy(), post.getCreatedBy())
                && Objects.equals(getCategory(), post.getCategory()) && Objects.equals(getCreationTime(),
                post.getCreationTime()) && Objects.equals(getLikedBy(), post.getLikedBy()) &&
                Objects.equals(getReplies(), post.getReplies()) && Objects.equals(getPostTags(),
                post.getPostTags());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTitle());
    }
}
