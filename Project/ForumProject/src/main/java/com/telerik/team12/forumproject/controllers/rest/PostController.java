package com.telerik.team12.forumproject.controllers.rest;

import com.telerik.team12.forumproject.controllers.AuthenticationHelper;
import com.telerik.team12.forumproject.exceptions.DuplicateEntityException;
import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.exceptions.UnauthorizedOperationException;
import com.telerik.team12.forumproject.models.Dtos.PostDto;
import com.telerik.team12.forumproject.models.Dtos.PostDtoAdvancedView;
import com.telerik.team12.forumproject.models.Dtos.PostDtoOut;
import com.telerik.team12.forumproject.models.Dtos.TagDto;
import com.telerik.team12.forumproject.models.FilterOptionsPosts;
import com.telerik.team12.forumproject.models.Post;
import com.telerik.team12.forumproject.models.Tag;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.services.contracts.PostService;
import com.telerik.team12.forumproject.services.mappers.PostMapper;
import com.telerik.team12.forumproject.services.mappers.TagMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/posts")

public class PostController {

    private final PostService postService;

    private final PostMapper postMapper;

    private final TagMapper tagMapper;
    private AuthenticationHelper authenticationHelper;

    @Autowired
    public PostController(PostService postService, PostMapper postMapper, TagMapper tagMapper, AuthenticationHelper authenticationHelper) {
        this.postService = postService;
        this.postMapper = postMapper;
        this.tagMapper = tagMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/filter")
    public List<PostDtoOut> filter(@RequestHeader HttpHeaders headers,
                                   @RequestParam(required = false) Optional<String> title,
                                   @RequestParam(required = false) Optional<String> keyword,
                                   @RequestParam(required = false) Optional<Integer> categoryId,
                                   @RequestParam(required = false) Optional<String> createdByUsername,
                                   @RequestParam(required = false) Optional<LocalDateTime> creationTime,
                                   @RequestParam(required = false) Optional<String> sortBy,
                                   @RequestParam(required = false) Optional<String> sortOrder) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            List<Post> result =
                    postService.filter
                            (new FilterOptionsPosts( keyword, categoryId, createdByUsername,
                                    creationTime, sortBy, sortOrder), user);
            return result.stream().map(postMapper::objectToDtoOut)
                    .collect(Collectors.toList());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
//        } catch (EntityNotFoundException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/latest")
    public List<PostDtoOut> getLatestTen() {
        List<Post> result = postService.getLatestTen();
        return result.stream().map(postMapper::objectToDtoOut)
                .collect(Collectors.toList());
    }

    @GetMapping("/hottest")
    public List<PostDtoOut> getHottestTen() {
        List<Post> result = postService.getHottestTen();
        return result.stream().map(postMapper::objectToDtoOut)
                .collect(Collectors.toList());
    }

    @GetMapping("/most-liked")
    public List<PostDtoOut> getTopTen() {
        List<Post> result = postService.getTopTen();
        return result.stream().map(postMapper::objectToDtoOut)
                .collect(Collectors.toList());
    }

    @GetMapping("/admin")
    public List<PostDtoAdvancedView> getAllForAdmins(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            List<Post> result = postService.getAllForAdmins(user);
            return result.stream().map(postMapper::objectToDtoAdvancedView)
                    .collect(Collectors.toList());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public PostDtoAdvancedView getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postService.getById(id, user);
            return postMapper.objectToDtoAdvancedView(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @PostMapping
    public PostDto create(@RequestHeader HttpHeaders headers, @Valid @RequestBody PostDto postDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postMapper.fromDto(postDTO);
            postService.createPost(post, user);
            return postMapper.objectToDTO(post);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Post update(@RequestHeader HttpHeaders headers,
                       @PathVariable int id, @Valid @RequestBody PostDto postDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Post post = postMapper.fromDto(postDto, id);
            postService.update(post, user);
            return post;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            postService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        } catch (UnauthorizedOperationException e) {
//            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
//        }
        }
    }

    @PutMapping("/toggle-like/{id}")
    public void toggleLike(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            postService.toggleLike(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/addTag/{id}")
    public void addTag(@RequestHeader HttpHeaders headers,
                       @PathVariable int id, @Valid @RequestBody TagDto tagDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Tag tag = tagMapper.fromDto(tagDto);
            postService.addTag(id, tag, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}


