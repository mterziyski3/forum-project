package com.telerik.team12.forumproject.services.contracts;

import com.telerik.team12.forumproject.models.FilterOptionsReplies;
import com.telerik.team12.forumproject.models.Post;
import com.telerik.team12.forumproject.models.Reply;
import com.telerik.team12.forumproject.models.User;

import java.util.List;

public interface ReplyService {

    List<Reply> getAllReplies(User user);

    Reply getById(int id);

    Reply create(Post post, Reply reply, User user);

    Reply update(Reply reply, User user);

    void delete(int id, User user);

    List<Reply> filter(User user ,FilterOptionsReplies filterOptionsReplies);
}
