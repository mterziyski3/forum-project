package com.telerik.team12.forumproject.controllers.rest;

import com.telerik.team12.forumproject.controllers.AuthenticationHelper;
import com.telerik.team12.forumproject.exceptions.DuplicateEntityException;
import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.exceptions.UnauthorizedOperationException;
import com.telerik.team12.forumproject.models.Category;
import com.telerik.team12.forumproject.models.Dtos.CategoryDto;
import com.telerik.team12.forumproject.models.Dtos.CategoryDtoOut;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.services.contracts.CategoryService;
import com.telerik.team12.forumproject.services.mappers.CategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/categories")
public class CategoryController {

    private final CategoryService categoryService;
    private final CategoryMapper categoryMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CategoryController(CategoryService categoryService, CategoryMapper categoryMapper, AuthenticationHelper authenticationHelper) {
        this.categoryService = categoryService;
        this.categoryMapper = categoryMapper;
        this.authenticationHelper = authenticationHelper;
    }

//    @GetMapping("/admin")
//    public List<CategoryDtoOut> getAllCategories(@RequestHeader HttpHeaders headers){
//        try {
//            User user = authenticationHelper.tryGetUser(headers);
//            return categoryService.getAllCategories(user).stream()
//                    .map(categoryMapper::toDisplayDto)
//                    .collect(Collectors.toList());
//        } catch (UnauthorizedOperationException e ) {
//            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
//        }
//    }

    @GetMapping("{id}")
    public CategoryDtoOut getById(@PathVariable int id) {
        try {
            Category category = categoryService.getById(id);
            return categoryMapper.objectToDTO(category);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public CategoryDto create(@RequestHeader HttpHeaders headers, @Valid @RequestBody CategoryDto categoryDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Category category = categoryMapper.dtoToObject(categoryDTO, new Category());
//            Category category = categoryMapper.dtoToObject(categoryDTO, new Category());
            categoryService.create(category, user);
            return categoryDTO;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("admin/{id}")
    public Category update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody CategoryDto categoryDTO) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Category categoryToUpdate = categoryService.getById(categoryDTO.getId(id));
            Category category = categoryMapper.dtoToObject(categoryDTO, categoryToUpdate);
            categoryService.update(category, user);
            return category;//don`t throw duplicate exception
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("admin/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            categoryService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

}
