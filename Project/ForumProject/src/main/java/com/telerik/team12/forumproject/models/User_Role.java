package com.telerik.team12.forumproject.models;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "user_roles")
public class User_Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id")
    private int id;

    @Column(name = "name")
    private String name;

    //@OneToMany(mappedBy = "role", cascade = CascadeType.ALL, orphanRemoval = true)
    @OneToMany(mappedBy = "role")
    private Set<User> likedBy;

    public User_Role() {
    }

    public User_Role(String name) {
        setName(name);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
