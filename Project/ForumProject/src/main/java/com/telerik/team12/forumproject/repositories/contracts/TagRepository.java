package com.telerik.team12.forumproject.repositories.contracts;

import com.telerik.team12.forumproject.models.FilterOptionsTags;
import com.telerik.team12.forumproject.models.Post;
import com.telerik.team12.forumproject.models.Tag;
import com.telerik.team12.forumproject.models.User;

import java.util.List;
import java.util.Set;

public interface TagRepository {
    List<Tag> getTags();

    Tag getTag(int id);

    Tag createTag(Tag tag);

    Tag updateTag(Tag tag);

    Tag deleteTag(int id);

    Tag getTagById(int id);

    Tag getTagByName(String name);

    Set<Post> getTaggedPostsByTagId(int tagId);

    List<Tag> filter(FilterOptionsTags filterOptionsTags);
}


