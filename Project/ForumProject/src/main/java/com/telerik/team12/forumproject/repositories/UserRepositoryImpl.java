package com.telerik.team12.forumproject.repositories;

import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.models.FilterOptionsUser;
import com.telerik.team12.forumproject.models.Post;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.models.User_Role;
import com.telerik.team12.forumproject.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final SessionFactory sessionFactory;
    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> filter(FilterOptionsUser filterOptions) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder("from User where 1=1");

            if (filterOptions.getFirstName().isPresent()) {
                queryString.append(" and firstName like :firstName");
            }
            if (filterOptions.getLastName().isPresent()) {
                queryString.append(" and lastName like :lastName");
            }
            if (filterOptions.getUserName().isPresent()) {
                queryString.append(" and username like :username");
            }

            addSorting(filterOptions, queryString);

            Query<User> query = session.createQuery(queryString.toString(), User.class);

            if (filterOptions.getFirstName().isPresent()) {
                // HQL: and name like %[some value from user]%
                query.setParameter("firstName", "%"+ filterOptions.getFirstName().get() + "%"); // %some-value%
            }
            if (filterOptions.getLastName().isPresent()) {
                query.setParameter("lastName", "%" + filterOptions.getLastName().get() + "%");
            }
            if (filterOptions.getUserName().isPresent()) {
                query.setParameter("username", "%" + filterOptions.getUserName().get() + "%");
            }

            return query.list();
        }
    }

    private void addSorting(FilterOptionsUser filterOptions, StringBuilder queryString) {
        if (filterOptions.getSortBy().isEmpty()) {
            return;
        }

        String orderBy = "";
        switch (filterOptions.getSortBy().get()) {
            case "firstName":
                orderBy = "firstName";
                break;
            case "lastName":
                orderBy = "lastName";
                break;
            case "username":
                orderBy = "username";
                break;
            default:
                return;
        }
        queryString.append(String.format(" order by %s", orderBy));

        if (filterOptions.getSortOrder().isPresent() && filterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            queryString.append(" desc");
        }
    }

    @Override
    public List<User> getUsers() {

        try(Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User", User.class);
                return query.list();
        }
    }



    @Override
    public User getUser(int id) {
        try(Session session = sessionFactory.openSession()) {
            User user = session.get(User.class, id);
            if(user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public User createUser(User user) {
        try(Session session = sessionFactory.openSession()) {
            session.save(user);
        }

        return user;
    }

    @Override
    public User updateUser(User user) {
        try(Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }

        return user;
    }

    @Override
    public User deleteUser(int id) {
        try(Session session = sessionFactory.openSession()) {
            User user = getUserById(id);

            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();

            return user;
        }
    }

    @Override
    public Set<Post> getUserLikedPosts(int id) {
        try(Session session = sessionFactory.openSession()){
            User user = session.get(User.class, id);

            if(user == null) {
                throw new EntityNotFoundException("User", id);
            }

            Set<Post> result = user.getLikedPosts();
            return result;
        }
    }

    @Override
    public void blockUser(int id) {
        try(Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            User user = getUserById(id);
            user.setIsBlocked(true);

            session.saveOrUpdate(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void makeAdmin(User userToUpdate) {
        try(Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(userToUpdate);
            session.getTransaction().commit();
        }
    }

    @Override
    public void unblockUser(int id) {
        try(Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            User user = getUserById(id);
            user.setIsBlocked(false);

            session.saveOrUpdate(user);
            session.getTransaction().commit();
        }
    }




    @Override
    public User getUserById(int id) {
        return getUser(id);
    }

    @Override
    public User getUserByUsername(String username) {
        try(Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where username = :username", User.class);
            query.setParameter("username", username);

            List<User> result = query.list();

            if(result.size() == 0) {
                throw new EntityNotFoundException("User", "username", username);
            }

            return result.get(0);
        }
    }

    @Override
    public User getUserByEmail(String email) {
        try(Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User where email = :email", User.class);
            query.setParameter("email", email);

            List<User> result = query.list();

            if(result.size() == 0) {
                throw new EntityNotFoundException("User", "email", email);
            }

            return result.get(0);
        }
    }

    private static boolean containsIgnoreCase(String value, String target) {
        return value.toLowerCase(Locale.ROOT).contains(target.toLowerCase());
    }
}
