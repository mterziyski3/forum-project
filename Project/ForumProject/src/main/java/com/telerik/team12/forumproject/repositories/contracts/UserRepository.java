package com.telerik.team12.forumproject.repositories.contracts;
import com.telerik.team12.forumproject.models.FilterOptionsUser;
import com.telerik.team12.forumproject.models.Post;
import com.telerik.team12.forumproject.models.User;

import java.util.List;
import java.util.Set;

public interface UserRepository {
    List<User> getUsers();

    User getUser(int id);

    User createUser(User user);

    User updateUser(User user);

    User deleteUser(int id);

    void unblockUser(int id);

    User getUserById(int id);

    User getUserByUsername(String username);

    User getUserByEmail(String email);

    List<User> filter(FilterOptionsUser filterOptions);

    Set<Post> getUserLikedPosts(int id);

    void blockUser(int id);

    void makeAdmin(User userToUpdate);
}
