package com.telerik.team12.forumproject.repositories.contracts;

import com.telerik.team12.forumproject.models.*;

import java.util.List;
import java.util.Set;

public interface PostRepository {

    List<Post> getAll();

    List<Post> filter(FilterOptionsPosts filterOptionsPosts);

    List<Post> getLatestTen();

    List<Post> getHottestTen();

    List<Post> getTopTen();

    Post getById(int id);

    Post getByTitle(String name);

    Post createPost(Post post);

    void update(Post post);

    void delete(int id);

    void toggleLike(int id, User user);

    List<Post> getPosts();

    Set<Reply> getRepliesByPostId(int postId);

    Set<Tag> getTagsByPostId(int postId);

    void toggleTag(int id, Tag tag);

}
