package com.telerik.team12.forumproject.repositories;

import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.models.FilterOptionsPosts;
import com.telerik.team12.forumproject.models.Post;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.repositories.contracts.CategoryRepository;
import com.telerik.team12.forumproject.repositories.contracts.PostRepository;
import com.telerik.team12.forumproject.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class PostRepositoryListImpl {

    private List<Post> posts;

    private final UserRepository userRepository;

    private final CategoryRepository categoryRepository;

    private static int id = 1;

    @Autowired
    public PostRepositoryListImpl
            (UserRepository userRepository, CategoryRepository categoryRepository) {
        this.userRepository = userRepository;
        this.categoryRepository = categoryRepository;
        posts = new ArrayList<>();
//        posts.add(new Post
//                (id++, "Mondial 2022 Finals", "Who will play in the finals?",
//                        userRepository.getUser(1), categoryRepository.getById(2), LocalDate.now()));
//        posts.add(new Post
//                (id++, "Neymar, Mbappe and Cristiano", "Who will be  Qatar 2022's striker",
//                        userRepository.getUser(1), categoryRepository.getById(2), LocalDate.now()));
    }
//    public List<Post> getAll(String title, String content,
//                             String authorFirstName, String authorLastName,
//                             String categoryName, LocalDate creationDate,
//                             String sortBy, String sortOrder) {
//        List<Post> result = new ArrayList<>(posts);
//        result = filterByTitle(result, title);
//        result = filterByContent(result, content);
//        result = filterByAuthorFirstName(result, authorFirstName);
//        result = filterByAuthorLastName(result, authorLastName);
//        result = filterByCategoryName(result, categoryName);
//        result = filterByCreationDate(result, creationDate);
//        result = sortBy(result, sortBy);
//        sortOrder(result, sortOrder);
//        return result;
//    }
    public List<Post>
    getAll() {
        return posts;
    }

    public List<Post> filter(FilterOptionsPosts filterOptionsPosts) {
        return null;
    }

    private void sortOrder(List<Post> result, String sortOrder) {
        if(result.isEmpty() || sortOrder==null||
                sortOrder.equalsIgnoreCase("asc")){
            return;
        }
        if(sortOrder.equalsIgnoreCase("desc")){
            Collections.reverse(result);
        }
    }

    private List<Post> sortBy(List<Post> result, String sortBy) {
        if(result.isEmpty() || sortBy==null){
            return result;
        }
        switch(sortBy){
            case "postTitle":
                result.sort(Comparator.comparing(Post::getTitle));
                break;
            case "postContent":
                result.sort(Comparator.comparing(Post::getContent));
                break;
            case "authorFirstName":
                result.sort(Comparator.comparing
                        (post -> post.getCreatedBy().getFirstName()));
                break;
            case "authorLastName":
                result.sort(Comparator.comparing
                        (post -> post.getCreatedBy().getLastName()));
                break;
            case "categoryName":
                result.sort(Comparator.comparing(post -> post.getCategory().getName()));
                break;
            case "creationDate":
                result.sort(Comparator.comparing(Post::getCreationTime));
                break;
        }
        return result;
    }

    private List<Post> publicSortBy(List<Post> result, String sortBy) {
        if(result.isEmpty() || sortBy==null){
            return result;
        }
        switch(sortBy){
            case "creationDate":
                result.sort(Comparator.comparing(Post::getCreationTime));
                sortOrder(result,"desc");
                break;

//            case "repliesCount":
//                result.sort(Comparator.comparing(Post::getRepliesCount));
//                break;
        }

        return result;
    }

    private List<Post> filterByCreationDate(List<Post> result, LocalDate creationDate) {
        if(result.isEmpty() || creationDate==null){
            return result;
        }
        return result.stream()
                .filter(post -> post.getCreationTime().equals(creationDate))
                .collect(Collectors.toList());
    }

    private List<Post> filterByCategoryName(List<Post> result, String categoryName) {
        if(result.isEmpty() || categoryName==null){
            return result;
        }
        return result.stream()
                .filter(post -> containsIgnoreCase(post.getCategory().getName(),
                        categoryName))
                .collect(Collectors.toList());
    }

    private List<Post> filterByAuthorLastName(List<Post> result, String authorLastName) {
        if(result.isEmpty() || authorLastName==null){
            return result;
        }
        return result.stream()
                .filter(post -> containsIgnoreCase
                        (post.getCreatedBy().getLastName(),
                        authorLastName))
                .collect(Collectors.toList());
    }

    private List<Post> filterByAuthorFirstName(List<Post> result, String authorFirstName) {
        if(result.isEmpty() || authorFirstName==null){
            return result;
        }
        return result.stream()
                .filter(post -> containsIgnoreCase(
                        post.getCreatedBy().getFirstName(),
                        authorFirstName))
                .collect(Collectors.toList());

    }

    private List<Post> filterByContent(List<Post> result, String content) {
        if(result.isEmpty() || content==null){
            return result;
        }
        return result.stream()
                .filter(post -> containsIgnoreCase(post.getContent(), content))
                .collect(Collectors.toList());
    }

    private List<Post> filterByTitle(List<Post> result, String title) {
        if(result.isEmpty() || title==null){
            return result;
        }
        return result.stream()
                .filter(post -> containsIgnoreCase(post.getTitle(), title))
                .collect(Collectors.toList());
    }

    private static boolean containsIgnoreCase(String value, String target) {
        return value.toLowerCase(Locale.ROOT).contains(target.toLowerCase(Locale.ROOT));
    }


    public Post getById(int id) {
        return posts.stream()
                .filter(post -> post.getId()==id)
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Post", id));
    }


    public Post getByTitle(String title) {
        return posts.stream()
                .filter(post -> post.getTitle().equals(title))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Post", "title", title));
    }


    public Post createPost(Post post) {
        posts.add(post);
        return post;
    }

    public Post update(Post post) {
        Post postToUpdate = getById(post.getId());
        postToUpdate.setTitle(post.getTitle());
    //    postToUpdate.setCategory(post.getCategory());
        postToUpdate.setContent(post.getContent());
     //   postToUpdate.setLikesCount(post.getLikesCount());
      //  postToUpdate.setLastUpdate(LocalDate.now());
        return postToUpdate;
    }


    public void delete(int id) {
        Post postToDelete = getById(id);
        posts.remove(postToDelete);

    }

    public void toggleLike(int id, User user) {
        Post post = getById(id);

        if(post.getLikedBy().contains(user)){
            post.getLikedBy().remove(user);
        }

        else {
            post.getLikedBy().add(user);
        }

    }


}
