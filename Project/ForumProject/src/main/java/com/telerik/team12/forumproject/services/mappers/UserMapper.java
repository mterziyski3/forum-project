package com.telerik.team12.forumproject.services.mappers;

import com.telerik.team12.forumproject.models.Dtos.RegisterDto;
import com.telerik.team12.forumproject.models.Dtos.UserDto;
import com.telerik.team12.forumproject.models.Dtos.UserFilterDto;
import com.telerik.team12.forumproject.models.FilterOptionsUser;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.models.User_Role;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserMapper {


    public User fromDto(UserDto userDto, User user) {
        user.setId(userDto.getId());
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setUsername(userDto.getUsername());
        user.setEmail(userDto.getEmail());
        user.setPassword("dasdsaads");

        return user;
    }

    public UserDto toDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setUsername(user.getUsername());
        userDto.setEmail(user.getEmail());

        return userDto;
    }

    public User fromRegisterDto(RegisterDto registerDto) {
        User user = new User();
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setEmail(registerDto.getEmail());

        user.setRole(new User_Role("Normal"));

        return user;
    }

    public FilterOptionsUser fromFilterDto(UserFilterDto userFilterDto) {
        FilterOptionsUser filterOptionsUser = new FilterOptionsUser(
                Optional.ofNullable(userFilterDto.getFirstName()),
                Optional.ofNullable(userFilterDto.getLastName()),
                Optional.ofNullable(userFilterDto.getUsername()),
//                Optional.ofNullable(userFilterDto.getEmail()),
//                Optional.ofNullable(userFilterDto.getRoleId()),
                Optional.ofNullable(userFilterDto.getSortBy()),
                Optional.ofNullable(userFilterDto.getSortOrder())
        );

        return filterOptionsUser;

    }
}
