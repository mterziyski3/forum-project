package com.telerik.team12.forumproject.services;

import com.telerik.team12.forumproject.exceptions.DuplicateEntityException;
import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.exceptions.UnauthorizedOperationException;
import com.telerik.team12.forumproject.models.Category;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.repositories.contracts.CategoryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import static com.telerik.team12.forumproject.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceImplTests {

    @Mock
    CategoryRepository mockRepository;

    @InjectMocks
    CategoryServiceImpl service;

    @Test
    public void getAll_should_callRepository() {
        // Arrange
        User mockUser = createMockAdmin();
        Mockito.when(mockRepository.getAllCategories())
                .thenReturn(new ArrayList<>());

        // Act
        service.getAllCategories();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllCategories();
    }

    @Test
    public void getAll_should_throw_when_userIsNotAdmin() {
        // Arrange
        User mockUser = createMockNormalUser();

        // Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getAllCategories());
    }


    @Test
    public void getById_should_returnCategory_when_matchExist() {
        // Arrange
        Category mockCategory = createMockCategory();

        Mockito.when(mockRepository.getById(mockCategory.getId()))
                .thenReturn(mockCategory);

        // Act
        Category result = service.getById(mockCategory.getId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCategory.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCategory.getName(), result.getName())
        );
    }

    @Test
    public void createCategory_should_throw_when_userIsNotAdmin() {

        User mockUser = createMockNormalUser();
        Category mockCategory = createMockCategory();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockCategory, mockUser));
    }

    @Test
    public void createCategory_should_throw_when_category_WithSameNameExists() {
        User mockUser = createMockAdmin();
        Category mockCategory = createMockCategory();

        Mockito.when(mockRepository.getByName(mockCategory.getName()))
                .thenReturn(mockCategory);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockCategory, mockUser));
    }

    @Test
    public void createCategory_should_callRepository_when_category_WithSameNameDoesNotExists() {
        User mockUser = createMockAdmin();
        Category mockCategory = createMockCategory();

        Mockito.when(mockRepository.getByName(mockCategory.getName()))
                .thenThrow(EntityNotFoundException.class);

        service.create(mockCategory, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockCategory);
    }

    @Test
    public void update_should_throwException_when_user_IsNotAdmin() {
        User mockUser = createMockNormalUser();
        Category mockCategory = createMockCategory();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockCategory, mockUser));
    }

    @Test
    public void update_should_callRepository_when_userIsAdmin() {
        User mockUser = createMockAdmin();
        Category mockCategory = createMockCategory();

        Mockito.when(mockRepository.getByName(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        service.update(mockCategory, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockCategory);
    }

    @Test
    public void update_should_throwException_when_categoryNameIsTaken() {
        User mockUser = createMockAdmin();
        Category mockCategory = createMockCategory();
        mockCategory.setName("test-name");
        Category anotherMockCategory = createMockCategory();
        mockCategory.setId(2);
        mockCategory.setName("test-name");

        Mockito.when(mockRepository.getByName(Mockito.anyString()))
                .thenReturn(anotherMockCategory);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockCategory, mockUser));
    }

    @Test
    public void update_should_callRepository_when_tryingToUpdateExistingCategory() {
        User mockUser = createMockAdmin();
        Category mockCategory = createMockCategory();

        Mockito.when(mockRepository.getByName(Mockito.anyString()))
                .thenReturn(mockCategory);

        service.update(mockCategory, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockCategory);
    }

    @Test
    public void delete_should_throwException_when_UserIsNotAdmin() {
        User mockUser = createMockNormalUser();
        Category mockCategory = createMockCategory();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockCategory.getId(), mockUser));
    }

    @Test
    public void delete_should_callRepository_when_initiatorIsAdmin() {
        User mockUser = createMockAdmin();
        Category mockCategory = createMockCategory();

        service.delete(mockCategory.getId(), mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockCategory.getId());
    }
}
