package com.telerik.team12.forumproject.services;

import com.telerik.team12.forumproject.exceptions.DuplicateEntityException;
import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.exceptions.UnauthorizedOperationException;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.models.User_Role;
import com.telerik.team12.forumproject.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {
    @Mock
    UserRepository mockRepository;

    @InjectMocks
    UserServiceImpl service;

    @Test
    public void getUsers_should_callRepository() {
        Mockito.when(mockRepository.getUsers())
                .thenReturn(new ArrayList<>()   );

        service.getUsers();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getUsers();
    }

    @Test
    public void getById_should_returnUser_when_matchExist() {
        User mockUser = createMockUser();

        Mockito.when(service.getUser(mockUser.getId()))
                .thenReturn(mockUser);

        User result = service.getUser(mockUser.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(mockUser.getLastName(), result.getLastName()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getId(), result.getId())
        );
    }

    @Test
    public void createUser_should_throw_when_userWithSameNameExists() {
        User mockUser = createMockUser();

        Mockito.when(service.getByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.createUser(mockUser));
    }

    @Test
    public void create_should_callRepository_when_beerWithSameNameDoesNotExist() {
        User mockUser = createMockUser();

        Mockito.when(mockRepository.getUserByEmail(mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        service.createUser(mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .createUser(mockUser);
    }

    @Test
    public void update_should_throwException_when_userIsNotCreatorOrAdmin() {
        User mockUser = createMockUser();
        User mockUpdator = createMockUser();
        mockUpdator.getRole().setName("Normal");

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.updateUser(mockUser, mockUpdator));
    }

    @Test
    void update_should_callRepository_when_userIsAdmin() {
        User mockUser = createMockUser();
        User mockUpdator = createMockUser();

        Mockito.when(mockRepository.getUserByEmail(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.verify(mockRepository, Mockito.times(1))
                .updateUser(mockUser);
    }

    @Test
    public void update_should_throwException_when_userNameIsTaken() {
        User mockUser = createMockUser();
        User mockUpdator = createMockUser();
        User anotherMockUser = createMockUser();

        anotherMockUser.setId(2);

        Mockito.when(mockRepository.getUserByEmail(mockUser.getEmail()))
                .thenReturn(anotherMockUser);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.updateUser(mockUser, mockUpdator));
    }

    @Test
    public void update_should_callRepository_when_tryingToUpdateExistingBeer() {
        User mockUser = createMockUser();
        User mockUpdator = createMockUser();
        mockUpdator.setId(2);

        Mockito.when(mockRepository.getUserByEmail(mockUser.getEmail()))
                .thenReturn(mockUser);

        service.updateUser(mockUser, mockUpdator);

        Mockito.verify(mockRepository, Mockito.times(1))
                .updateUser(mockUser);
    }

    @Test
    void delete_should_callRepository_when_initiatorIsAdmin() {
        User mockUser = createMockUser();
        User mockUpdator = createMockUser();
        mockUpdator.setId(2);

        service.deleteUser(mockUser.getId(), mockUpdator);

        Mockito.verify(mockRepository, Mockito.times(1)).
                deleteUser(mockUser.getId());
    }

    @Test
    void delete_should_throw_when_initiatorIsNotAdmin() {
        User user = createMockUser();
        User updator = createMockUser();
        updator.getRole().setName("Normal");
        updator.setId(2);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.deleteUser(user.getId(), updator));
    }

    private User createMockUser() {
        User mockUser = new User();
        mockUser.setFirstName("test");
        mockUser.setLastName("test");
        mockUser.setUsername("test");
        mockUser.setEmail("test");
        mockUser.setId(1);

        User_Role role = new User_Role();
        role.setName("Admin");

        mockUser.setRole(role);
        return mockUser;
    }
}
