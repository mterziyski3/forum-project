package com.telerik.team12.forumproject.services;

import com.telerik.team12.forumproject.exceptions.DuplicateEntityException;
import com.telerik.team12.forumproject.exceptions.EntityNotFoundException;
import com.telerik.team12.forumproject.exceptions.UnauthorizedOperationException;
import com.telerik.team12.forumproject.models.Reply;
import com.telerik.team12.forumproject.models.User;
import com.telerik.team12.forumproject.repositories.contracts.ReplyRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.telerik.team12.forumproject.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ReplyServiceImplTests {

    @Mock
    ReplyRepository mockRepository;

    @InjectMocks
    ReplyServiceImpl service;

    @Test
    public void getAll_should_callRepository() {
        User mockUser = createMockAdmin();

        Mockito.when(mockRepository.getAllReplies())
                .thenReturn(new ArrayList<>());

        service.getAllReplies(mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllReplies();
    }

    @Test
    public void getAll_should_throw_when_userIsNotAdmin() {
        User mockUser = createMockNormalUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.getAllReplies(mockUser));
    }

    @Test
    public void getById_should_returnReply_when_matchExist() {
        Reply mockReply = createMockReply();

        Mockito.when(mockRepository.getById(mockReply.getId()))
                .thenReturn(mockReply);

        Reply result = service.getById(mockReply.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockReply.getId(), result.getId()),
                () -> Assertions.assertEquals(mockReply.getContent(), result.getContent()),
                () -> Assertions.assertEquals(mockReply.getLikes(), result.getLikes()),
                () -> Assertions.assertEquals(mockReply.getCreatedBy(), result.getCreatedBy()),
                () -> Assertions.assertEquals(mockReply.getCreationTime(), result.getCreationTime())
        );
    }

    @Test
    public void createReply_should_throw_when_userIsNotAdminOrNormalUser() {
        User mockUser = createMockGuest();
        Reply mockReply = createMockReply();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockReply, mockUser));
    }

    @Test
    public void createReply_should_throw_when_userIsBlocked() {
        User mockUser = createMockNormalUser();
        Reply mockReply = createMockReply();
        mockUser.setIsBlocked(true);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockReply, mockUser));
    }

    @Test
    public void createReply_should_throw_when_reply_WithSameContentExists() {
        User mockUser = createMockNormalUser();
        Reply mockReply = createMockReply();

        Mockito.when(mockRepository.getByContent(mockReply.getContent()))
                .thenReturn(mockReply);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockReply, mockUser));
    }

    @Test
    public void createReply_should_callRepository_when_reply_WithSameContentDoesNotExists() {
        User mockUser = createMockNormalUser();
        Reply mockReply = createMockReply();

        Mockito.when(mockRepository.getByContent(mockReply.getContent()))
                .thenThrow(EntityNotFoundException.class);

        service.create(mockReply, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockReply);
    }

    @Test
    public void update_should_callRepository_when_userNotIsAdminOrCreator() {
        User mockUser = createMockNormalUser();
        Reply mockReply = createMockReply();

        mockReply.setCreatedBy(mockUser);

        User mockUse1 = createMockNormalUser();
        mockUse1.setUsername("TestUser");


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockReply, mockUse1));
    }

    @Test
    public void update_should_callRepository_when_userIsAdminOrCreator() {
        User mockUser = createMockNormalUser();
        Reply mockReply = createMockReply();

        Mockito.when(mockRepository.getByContent(Mockito.anyString()))
                .thenThrow(EntityNotFoundException.class);

        service.update(mockReply, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockReply);
    }

    @Test
    public void update_should_throwException_when_replyContentIsTaken() {
        User mockUser = createMockAdmin();
        Reply mockReply = createMockReply();
        mockReply.setContent("test-content");
        Reply anotherMockReply = createMockReply();
        anotherMockReply.setId(2);
        anotherMockReply.setContent("test-content");

        Mockito.when(mockRepository.getByContent(Mockito.anyString()))
                .thenReturn(anotherMockReply);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockReply, mockUser));
    }

    @Test
    public void update_should_callRepository_when_tryingToUpdateExistingReply() {
        User mockUser = createMockAdmin();
        Reply mockReply = createMockReply();

        Mockito.when(mockRepository.getByContent(Mockito.anyString()))
                .thenReturn(mockReply);

        service.update(mockReply, mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockReply);
    }

    @Test
    void delete_should_throwException_when_UserIsNotAdminOrCreator() {
        User mockUser = createMockGuest();
        Reply mockReply = createMockReply();

        mockReply.setCreatedBy(mockUser);

        User mockUse1 = createMockNormalUser();
        mockUse1.setUsername("TestUser");

        Mockito.when(mockRepository.getById(mockReply.getId()))
                        .thenReturn(mockReply);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockReply.getId(), mockUse1));
    }

    @Test
    public void deleteReply_should_throw_when_userIsBlocked() {
        User mockUser = createMockNormalUser();
        Reply mockReply = createMockReply();
        mockUser.setIsBlocked(true);

        Mockito.when(mockRepository.getById(mockReply.getId()))
                .thenReturn(mockReply);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockReply.getId(), mockUser));
    }


    @Test
    void delete_should_callRepository_when_initiatorIsAdminOrCreator() {
        User mockUser = createMockAdmin();
        Reply mockReply = createMockReply();

        Mockito.when(mockRepository.getById(mockReply.getId()))
                .thenReturn(mockReply);

        service.delete(mockReply.getId(), mockUser);

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockReply.getId());
    }
}
