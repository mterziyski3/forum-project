package com.telerik.team12.forumproject;

import com.telerik.team12.forumproject.models.*;

import java.time.LocalDateTime;

public class Helpers {

    public static User createMockAdmin() {
        return createMockUser("Admin");
    }

    public static User createMockNormalUser() {
        return createMockUser("Normal");
    }

    public static User createMockGuest() {
        return createMockUser("Guest");
    }

    public static User createMockUser(String role) {
            var mockUser = new User();
            mockUser.setId(1);
            mockUser.setFirstName("MockFirstName");
            mockUser.setLastName("MockLastName");
            mockUser.setUsername("MockUsername");
            mockUser.setPassword("MockPassword");
            mockUser.setEmail("gosho39@abv.bg");
            mockUser.setIsBlocked(false);
            mockUser.setRole((createMockRole(role)));

            return mockUser;
    }

    public static User_Role createMockRole(String role) {
            var mockRole = new User_Role();
            mockRole.setId(1);
            mockRole.setName(role);
            return mockRole;
    }

    public static Category createMockCategory() {
            var mockCategory = new Category();
            mockCategory.setId(1);
            mockCategory.setName("TestCategory");
            return mockCategory;
    }

    public static Reply createMockReply() {
        var createMockReply = new Reply();
        createMockReply.setId(1);
        createMockReply.setContent("TestReply");
        createMockReply.setLikes(5);
        createMockReply.setCreatedBy(createMockAdmin());
        createMockReply.setCreationTime(createMockReply.getCreationTime());
        return createMockReply;
    }



}
